package ru.nsu.fit.g15203.vorobyeva.panels;


import javax.swing.border.AbstractBorder;
import java.awt.*;
import java.awt.image.BufferedImage;

import ru.nsu.fit.g15203.vorobyeva.MyImage;


public class BorderPanel extends AbstractBorder {
//    private MyImage image;
//    private MyImage.ImageColor[][] bitmap;
    private int selectX;
    private int selectY;
    private BufferedImage bufferedImage;

    public BorderPanel(MyImage imageBmp, int selectX, int selectY, BufferedImage bufferedImage){
//        this.image = imageBmp;
//        this.bitmap = imageBmp.getBitMap();
        this.selectX = selectX;
        this.selectY = selectY;
        this.bufferedImage = bufferedImage;
    }


    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        super.paintBorder(c, g, x, y, width, height);
        Graphics2D g2d = (Graphics2D) g;
        int count = 0;
        boolean stroke = false;


        for (int i = 0; i < height; i++) {
            if (!stroke) {
                Color color = new Color(bufferedImage.getRGB(x + i + selectX,y + selectY));
                g2d.setColor(new Color(255 - color.getRed(),
                        255 - color.getGreen(),
                        255 - color.getBlue()));
                g2d.drawLine(x + i, y, x + i, y);
                color = new Color(bufferedImage.getRGB(x + i + selectX, y + width - 1  + selectY));
                g2d.setColor(new Color(255 - color.getRed(),
                        255 - color.getGreen(),
                        255 - color.getBlue()));
                g2d.drawLine(x + i, y + width - 1, x + i, y + width - 1);
                count++;
                stroke = count % 10 == 0;
            }
            else {
                Color color = new Color(bufferedImage.getRGB(x + i + selectX,y + selectY));
                g2d.setColor(color);
                g2d.drawLine(x + i, y, x + i, y);
                color = new Color(bufferedImage.getRGB(x + i + selectX, y + width - 1  + selectY));
                g2d.setColor(color);
                g2d.drawLine(x + i, y + width - 1, x + i, y + width - 1);
                count++;

                if (count % 10 == 0) {
                    stroke = false;
                    count = 0;
                }
            }
        }
        for (int i = 0; i < width; i++) {
            if (!stroke) {
                Color color = new Color(bufferedImage.getRGB(x + selectX, y + i + selectY));
                g2d.setColor(new Color(255 - color.getRed(),
                        255 - color.getGreen(),
                        255 - color.getBlue()));
                g2d.drawLine(x, y + i, x, y + i);
                color = new Color(bufferedImage.getRGB(x + height - 1 + selectX, y + i + selectY));
                g2d.setColor(new Color(255 - color.getRed(),
                        255 - color.getGreen(),
                        255 - color.getBlue()));
                g2d.drawLine(x + height - 1, y + i, x + height - 1, y + i);
                count++;
                stroke = count % 10 == 0;
            }
            else {
                Color color = new Color(bufferedImage.getRGB(x + selectX, y + i + selectY));
                g2d.setColor(color);
                g2d.drawLine(x, y + i, x, y + i);
                color = new Color(bufferedImage.getRGB(x + height - 1 + selectX, y + i + selectY));
                g2d.setColor(color);
                g2d.drawLine(x + height - 1, y + i, x + height - 1, y + i);
                count++;
                if (count % 20 == 0) {
                    stroke = false;
                    count = 0;
                }
            }
        }
    }
}
