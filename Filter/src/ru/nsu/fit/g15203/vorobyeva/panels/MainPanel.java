package ru.nsu.fit.g15203.vorobyeva.panels;

import ru.nsu.fit.g15203.vorobyeva.filters.*;
import ru.nsu.fit.g15203.vorobyeva.MyImage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class MainPanel extends JPanel{
    public boolean imageLoaded;
    protected MyImage image;
    public JPanel select;
    public boolean enabled = false;
    public ImagePanel zoneAPanel;
    public ImagePanel zoneBPanel;
    public ImagePanel zoneCPanel;

    static protected int subPanelSize = 350;

    public MainPanel() {
        CustomListener customListener = new CustomListener();

        Container pane = new Container();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        select = new JPanel();
        select.setOpaque(false);

        imageLoaded = false;

        zoneAPanel = new ImagePanel();
        zoneAPanel.addMouseMotionListener(customListener);
        zoneAPanel.addMouseListener(customListener);
        zoneAPanel.add(select);

        JPanel zoneABorderPanel = new JPanel(new GridLayout(1, 1));
        zoneABorderPanel.setBorder((BorderFactory.createDashedBorder
                (Color.BLACK, 1, 6, 3, true)));//,

        zoneABorderPanel.setPreferredSize(new Dimension(subPanelSize, subPanelSize));
        zoneABorderPanel.add(zoneAPanel);
        add(zoneABorderPanel, pane);
        add(Box.createRigidArea(new Dimension(10,0)));

        zoneBPanel = new ImagePanel();
        JPanel zoneBBorderPanel = new JPanel(new GridLayout(1, 1));
        zoneBBorderPanel.setBorder(BorderFactory.createDashedBorder
                (Color.BLACK, 1, 6, 3, true));
        zoneBBorderPanel.setPreferredSize(new Dimension(subPanelSize, subPanelSize));
        zoneBBorderPanel.add(zoneBPanel);
        add(zoneBBorderPanel, pane);
        add(Box.createRigidArea(new Dimension(10,0)));

        zoneCPanel = new ImagePanel();
        JPanel zoneCBorderPanel = new JPanel(new GridLayout(1, 1));
        zoneCBorderPanel.setBorder(BorderFactory.createDashedBorder
                (Color.BLACK, 1, 6, 3, true));
        zoneCBorderPanel.setPreferredSize(new Dimension(subPanelSize, subPanelSize));
        zoneCBorderPanel.add(zoneCPanel);
        add(zoneCBorderPanel, pane);
    }

    public boolean isImageLoaded() {
        return imageLoaded;
    }

    public void setImage(MyImage image) {
        this.image = image;
        zoneAPanel.setImage(image);

        revalidate();
        repaint();
    }

    public void reset(){
        zoneAPanel.setImage(null);
        zoneBPanel.setImage(null);
        zoneCPanel.setImage(null);
        select.setVisible(false);

        revalidate();
        repaint();
    }

    public void copyFromC() {
        if (zoneCPanel.getImage() == null)
            return;

        MyImage copyImage = new MyImage(zoneCPanel.getImage());
        zoneBPanel.setImage(copyImage);
        zoneBPanel.repaint();
    }

    public void copyFromB() {
        if (zoneBPanel.getImage() == null)
            return;

        MyImage copyImage = new MyImage(zoneBPanel.getImage());
        zoneCPanel.setImage(copyImage);
        zoneCPanel.repaint();
    }
    private class CustomListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            if (zoneAPanel.getImage() == null || !enabled)
                return;
            imageLoaded = true;

            setSelect(e);

            float k = (float) (image.getWidth()) / subPanelSize;
            int x = (int) (e.getX() * k) - subPanelSize / 2;
            int y = (int) (e.getY() * k) - subPanelSize / 2;

            if (x < 0) {
                x = 0;
            } else if (x + subPanelSize > image.getWidth()) {
                x = image.getWidth() - subPanelSize;
            }

            if (y < 0) {
                y = 0;
            } else if (y + subPanelSize > image.getHeight()) {
                y = image.getHeight() - subPanelSize;
            }

            zoneBPanel.setImage(image.copyPart(x, y, subPanelSize, subPanelSize));
            zoneBPanel.repaint();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (zoneAPanel.getImage() == null || !enabled)
                return;
            imageLoaded = true;

            setSelect(e);

            float k = (float) (image.getWidth()) / subPanelSize;
            int x = (int) (e.getX() * k) - subPanelSize / 2;
            int y = (int) (e.getY() * k) - subPanelSize / 2;
            if (x < 0) {
                x = 0;
            } else if (x + subPanelSize > image.getWidth()) {
                x = image.getWidth() - subPanelSize;
            }

            if (y < 0) {
                y = 0;
            } else if (y + subPanelSize > image.getHeight()) {
                y = image.getHeight() - subPanelSize;
            }

            zoneBPanel.setImage(image.copyPart(x, y, subPanelSize, subPanelSize));
            zoneBPanel.repaint();
        }
    }



    private void setSelect(MouseEvent e){
        int selectX = e.getPoint().x - select.getWidth() / 2;
        int selectY = e.getPoint().y - select.getWidth() / 2;
        select.setPreferredSize(new Dimension(zoneAPanel.getSelect(), zoneAPanel.getSelect()));

        if (selectX < 0)
            selectX = 0;
        if (selectX > 350 - zoneAPanel.getSelect())
            selectX = 350 - zoneAPanel.getSelect();
        if (selectY < 0)
            selectY = 0;
        if (selectY > 350 - zoneAPanel.getSelect())
            selectY =  350 - zoneAPanel.getSelect();

        BorderPanel borderPanel = new BorderPanel(zoneAPanel.getImage(), selectX, selectY, zoneAPanel.getBorderImage());
        select.setBorder(borderPanel);

        select.setLocation(selectX, selectY);
        select.setOpaque(false);
        select.setVisible(true);
    }

    public void blur() {
        FilterType blur = new GaussBlur();
        MyImage filteredImage = blur.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void sharpness() {
        FilterType sharpness = new Sharpness();
        MyImage filteredImage = sharpness.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void aquarelle() {
        FilterType aqua = new Watercolor();
        MyImage filteredImage = aqua.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void doubleSize() {
        FilterType ds = new DoubleSize();
        MyImage filteredImage = ds.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void floydSteinberg(int r, int g, int b) {
        FilterType floyd = new FloydSteinberg(r, g, b);
        MyImage filteredImage = floyd.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void negative() {
        FilterType negative = new Negative();
        MyImage filteredImage = negative.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void grey() {
        FilterType grey = new GreyShades();
        MyImage filteredImage = grey.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void orderedDithering() {
        FilterType orderedDithering = new OrderedDithering();
        System.out.println("im here");
        MyImage filteredImage = orderedDithering.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void robertsOperator(int blackness) {
        FilterType roberts = new RobertsCross();
        FilterType mono = new Monochrome(blackness);

        MyImage filteredImage = roberts.apply(zoneBPanel.getImage());
        filteredImage = mono.apply(filteredImage);

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void sobelOperator(int blackness) {
        FilterType sobel = new SobelOperator();
        FilterType mono = new Monochrome(blackness);

        MyImage filteredImage = sobel.apply(zoneBPanel.getImage());
        filteredImage = mono.apply(filteredImage);

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void stamp() {
        FilterType stamp = new Stamp();
        MyImage filteredImage = stamp.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void monochrome(int blackness) {
        FilterType mono = new Monochrome(blackness);
        MyImage filteredImage = mono.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void gamma(int g) {
        FilterType gamma = new Gamma(g);
        MyImage filteredImage = gamma.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void rotation(int angle, MainPanel panel) {
        FilterType rotation = new Rotation(angle, panel);
        MyImage filteredImage = rotation.apply(zoneBPanel.getImage());

        zoneCPanel.setImage(filteredImage);
        zoneCPanel.repaint();
    }

    public void cancel() {
        if (!imageLoaded)
            return;

        zoneCPanel.setImage(new MyImage(zoneBPanel.getImage()));
        zoneCPanel.repaint();
    }
}