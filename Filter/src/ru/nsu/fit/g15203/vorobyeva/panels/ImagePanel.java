package ru.nsu.fit.g15203.vorobyeva.panels;

import ru.nsu.fit.g15203.vorobyeva.MyImage;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class ImagePanel extends JPanel {
    protected MyImage image;
    private BufferedImage borderImage;
    private int select;

    public ImagePanel() {}

    public MyImage getImage() {
        return image;
    }

    public void setImage(MyImage image) {
        this.image = image;
    }

    public int getSelect(){
//        System.out.println("select "+ select);
        return select;
    }

    public BufferedImage getBorderImage(){
        return borderImage;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (image == null) {
            return;
        }

        MyImage.ImageColor[][] bitmap = image.getBitMap();
        int height = image.getHeight();
        int width = image.getWidth();

        select = 350 * 350;
        select = height > width ?
                 select / height:
                 select / width;

        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        for (int i = 1; i < height; i++) {
            for (int j = 1; j < width; j++) {
                Color color = new Color(bitmap[i][j].red, bitmap[i][j].green, bitmap[i][j].blue);
                bufferedImage.setRGB(j, i, color.getRGB());
            }
        }

        float resize = (float)(width) / height;

        if (resize >= 1) {
            g.drawImage(bufferedImage, 0, 0,
                    MainPanel.subPanelSize, (int) (MainPanel.subPanelSize / resize), null);
            Image image = bufferedImage.getScaledInstance
                    (MainPanel.subPanelSize, (int) (MainPanel.subPanelSize / resize), Image.SCALE_SMOOTH);
            borderImage = toBufferedImage(image);
        }

        else {
            g.drawImage(bufferedImage, 0, 0,
                    (int) (MainPanel.subPanelSize * resize), MainPanel.subPanelSize, null);
            Image image = bufferedImage.getScaledInstance
                    (MainPanel.subPanelSize, (int) (MainPanel.subPanelSize / resize), Image.SCALE_SMOOTH);
            borderImage = toBufferedImage(image);
        }

    }

    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        BufferedImage bufferedImage = new BufferedImage
                (img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics2D bGr = bufferedImage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        return bufferedImage;
    }
}
