package ru.nsu.fit.g15203.vorobyeva.filters;

import ru.nsu.fit.g15203.vorobyeva.MyImage;

public class FloydSteinberg implements FilterType {
    private int[] redBits;
    private int[] greenBits;
    private int[] blueBits;
    private int palletteRed;
    private int palletteGreen;
    private int palletteBlue;

    public FloydSteinberg(int redBits, int greenBits, int blueBits) {
        this.palletteRed = redBits;
        this.palletteGreen = greenBits;
        this.palletteBlue = blueBits;
    }

    @Override
    public MyImage apply(MyImage image) {
        MyImage filteredImage = new MyImage(image);
        MyImage.ImageColor[][] filteredBitmap = filteredImage.getBitMap();
        MyImage.ImageColor[][] bitmap = filteredImage.getBitMap();
        MyImage.ImageColor[][] tempBitmap = filteredImage.getBitMap();

        redBits = new int[palletteRed];
        for (int i = 0; i < palletteRed; i++) {
            redBits[i] = (256 / (palletteRed - 1)) * i;
        }
        redBits[palletteRed - 1] = 256;

        greenBits = new int[palletteGreen];
        for (int i = 0; i < palletteGreen; i++) {
            greenBits[i] = (256 / (palletteGreen - 1)) * i;
        }
        greenBits[palletteGreen - 1] = 256;

        blueBits = new int[palletteBlue];
        for (int i = 0; i < palletteBlue; i++) {
            blueBits[i] = (256 / (palletteBlue - 1)) * i;
        }
        blueBits[palletteBlue - 1] = 256;

        for (int i = 0; i < bitmap.length; i++) {
            for (int j = 0; j < bitmap[0].length; j++) {
                MyImage.ImageColor oldColor = new MyImage.ImageColor(tempBitmap[i][j].red, tempBitmap[i][j].green, tempBitmap[i][j].blue);
                MyImage.ImageColor newColor = findDef(oldColor.red, oldColor.green, oldColor.blue);

                if (newColor.red > 255)
                    newColor.red = 255;
                if (newColor.green > 255)
                    newColor.green = 255;
                if (newColor.blue > 255)
                    newColor.blue = 255;

                if (newColor.green < 0)
                    newColor.green = 0;
                if (newColor.red < 0)
                    newColor.red = 0;
                if (newColor.blue < 0)
                    newColor.blue = 0;

                filteredBitmap[i][j].red = newColor.red;
                filteredBitmap[i][j].green = newColor.green;
                filteredBitmap[i][j].blue = newColor.blue;

                int errRed = oldColor.red - newColor.red;
                int errGreen = oldColor.green - newColor.green;
                int errBlue = oldColor.blue - newColor.blue;

                MyImage.ImageColor error = new MyImage.ImageColor(errRed, errGreen, errBlue);

                if (j + 1 < bitmap[0].length)
                    calculateErr(tempBitmap[i][j + 1], 7./ 16, error);
                if (j - 1 >= 0 && i + 1 < bitmap.length)
                    calculateErr(tempBitmap[i + 1][j - 1], 3./ 16, error);
                if (i + 1 < bitmap.length)
                    calculateErr(tempBitmap[i + 1][j], 5./ 16, error);
                if (j + 1 < bitmap[0].length && i + 1 < bitmap.length)
                    calculateErr(tempBitmap[i + 1][j + 1], 1./ 16, error);
            }
        }

        return filteredImage;
    }

    private void calculateErr(MyImage.ImageColor rgb, double difErr, MyImage.ImageColor error){
        rgb.red += error.red * difErr;
        rgb.green += error.green * difErr;
        rgb.blue += error.blue * difErr;
    }

    private MyImage.ImageColor findDef(int red, int green, int blue) {
        int maxDif = 0x00FFFFFF;
        int min = 0;
        for (int i = 0; i < redBits.length; i++) {
            if (Math.abs(redBits[i] - red) < maxDif) {
                maxDif = Math.abs(redBits[i] - red);
                min = i;
            }
        }
        int newRed = redBits[min];
        maxDif = 0x00FFFFFF;
        min = 0;

        for (int i = 0; i < greenBits.length; i++) {
            if (Math.abs(greenBits[i] - green) < maxDif) {
                maxDif = Math.abs(greenBits[i] - green);
                min = i;
            }
        }
        int newGreen = greenBits[min];
        maxDif = 0x00FFFFFF;
        min = 0;

        for (int i = 0; i < blueBits.length; i++) {
            if (Math.abs(blueBits[i] - blue) < maxDif) {
                maxDif = Math.abs(blueBits[i] - blue);
                min = i;
            }
        }
        int newBlue = blueBits[min];

        return new MyImage.ImageColor(newRed, newGreen, newBlue);
    }
}

