package ru.nsu.fit.g15203.vorobyeva.filters;

import ru.nsu.fit.g15203.vorobyeva.MyImage;

public interface FilterType {
    MyImage apply(final MyImage image);
}
