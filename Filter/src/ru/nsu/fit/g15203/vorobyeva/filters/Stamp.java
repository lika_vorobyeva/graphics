package ru.nsu.fit.g15203.vorobyeva.filters;

import ru.nsu.fit.g15203.vorobyeva.MyImage;

public class Stamp implements FilterType {
    @Override
    public MyImage apply(MyImage image) {
        FilterType grey = new GreyShades();
        image = grey.apply(image);

        MyImage filteredImage = new MyImage(image);
        MyImage.ImageColor[][] bitmap = image.getBitMap();
        MyImage.ImageColor[][] filteredBitmap = filteredImage.getBitMap();

        for (int i = 1; i < filteredImage.getHeight() - 1; i++) {
            for (int j = 1; j < filteredImage.getWidth() - 1; j++) {
                int r = bitmap[i - 1][j].red -
                        bitmap[i][j + 1].red +
                        bitmap[i][j + 1].red -
                        bitmap[i + 1][j].red;
                r += 128;

                int g = bitmap[i - 1][j].green -
                        bitmap[i][j + 1].green +
                        bitmap[i][j + 1].green -
                        bitmap[i + 1][j].green;
                g += 128;

                int b = bitmap[i - 1][j].blue -
                        bitmap[i][j + 1].blue +
                        bitmap[i][j + 1].blue -
                        bitmap[i + 1][j].blue;
                b += 128;

                if (r < 0)
                    r = 0;
                if (g < 0)
                    g = 0;
                if (b < 0)
                    b = 0;

                if (r > 255)
                    r = 255;
                if (g > 255)
                    g = 255;
                if (b > 255)
                    b = 255;

                filteredBitmap[i][j].red = r;
                filteredBitmap[i][j].green = g;
                filteredBitmap[i][j].blue = b;
            }
        }

        return filteredImage;
    }
}
