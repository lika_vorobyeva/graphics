package ru.nsu.fit.g15203.vorobyeva.filters;

import ru.nsu.fit.g15203.vorobyeva.MyImage;


public class OrderedDithering implements FilterType {
    private int redBits = 1 << 2;
    private int greenBits = 1 << 2;
    private int blueBits = 1 << 2;


    @Override
    public MyImage apply(MyImage image) {
        MyImage filteredImage = new MyImage(image);
        MyImage.ImageColor[][] filteredBitmap = filteredImage.getBitMap();

        double[][] core = {
                {1, 49, 13, 61, 4, 52, 16, 64},
                {33, 17, 45, 29, 36, 20, 48, 32},
                {9, 57, 5, 53, 12, 60, 8, 56},
                {41, 25, 37, 21, 44, 28, 40, 24},
                {3, 51, 15, 63, 2, 50, 14, 62},
                {35, 19, 47, 31, 34, 18, 46, 30},
                {11, 59, 7, 55, 10, 58, 6, 54},
                {43, 47, 39, 23, 42, 26, 38, 22},
        };
        int mod = core.length;

        int redGap = 256 / redBits;
        int greenGap = 256 / greenBits;
        int blueGap = 256 / blueBits;

        for (int i = 1; i < filteredImage.getHeight(); i++) {
            for (int j = 1; j < filteredImage.getWidth(); j++) {
                int er = filteredBitmap[i][j].red % redGap;
                int eg = filteredBitmap[i][j].green % greenGap;
                int eb = filteredBitmap[i][j].blue % blueGap;

                filteredBitmap[i][j].red -= er;
                filteredBitmap[i][j].green -= eg;
                filteredBitmap[i][j].blue -= eb;

                if (er > core[(i - 1) % mod][(j - 1) % mod])
                    filteredBitmap[i][j].red += redGap;
                if (eg > core[(i - 1) % mod][(j - 1) % mod])
                    filteredBitmap[i][j].green += greenGap;
                if (eb > core[(i - 1) % mod][(j - 1) % mod])
                    filteredBitmap[i][j].blue += blueGap;

                if (filteredBitmap[i][j].red > 255)
                    filteredBitmap[i][j].red = 255;
                if (filteredBitmap[i][j].green > 255)
                    filteredBitmap[i][j].green = 255;
                if (filteredBitmap[i][j].blue > 255)
                    filteredBitmap[i][j].blue = 255;
            }
        }

        return filteredImage;
    }
}

