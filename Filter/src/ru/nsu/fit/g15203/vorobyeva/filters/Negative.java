package ru.nsu.fit.g15203.vorobyeva.filters;

import ru.nsu.fit.g15203.vorobyeva.MyImage;

public class Negative implements FilterType {
    @Override
    public MyImage apply(MyImage image) {
        MyImage filteredImage = new MyImage(image);
        MyImage.ImageColor[][] filteredBitmap = filteredImage.getBitMap();

        for (int i = 0; i < filteredImage.getHeight(); i++) {
            for (int j = 0; j < filteredImage.getWidth(); j++) {
                filteredBitmap[i][j].red = 255 - filteredBitmap[i][j].red;
                filteredBitmap[i][j].green = 255 - filteredBitmap[i][j].green;
                filteredBitmap[i][j].blue = 255 - filteredBitmap[i][j].blue;
            }
        }
        return filteredImage;
    }
}
