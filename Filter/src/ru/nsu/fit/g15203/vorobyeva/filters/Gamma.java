package ru.nsu.fit.g15203.vorobyeva.filters;


import ru.nsu.fit.g15203.vorobyeva.MyImage;

public class Gamma implements FilterType {
    private double gamma_new;

    public Gamma(double gamma) {
        gamma = gamma / 10;
        gamma_new = 1 / gamma;
    }

    @Override
    public MyImage apply(MyImage image) {

        MyImage filteredImage = new MyImage(image);

        MyImage.ImageColor[][] filteredBitmap = filteredImage.getBitMap();

        for (int i = 0; i < filteredImage.getHeight(); i++) {
            for (int j = 0; j < filteredImage.getWidth(); j++) {
                filteredBitmap[i][j].red = (int) (255 * (Math.pow((double) filteredBitmap[i][j].red / (double) 255, gamma_new)));
                filteredBitmap[i][j].green = (int) (255 * (Math.pow((double) filteredBitmap[i][j].green / (double) 256, gamma_new)));
                filteredBitmap[i][j].blue  = (int) (255 * (Math.pow((double) filteredBitmap[i][j].blue / (double) 255, gamma_new)));

                if (filteredBitmap[i][j].red > 255)
                    filteredBitmap[i][j].red = 255;
                if (filteredBitmap[i][j].green > 255)
                    filteredBitmap[i][j].green = 255;
                if (filteredBitmap[i][j].blue > 255)
                    filteredBitmap[i][j].blue = 255;

                if (filteredBitmap[i][j].red < 0)
                    filteredBitmap[i][j].red = 0;
                if (filteredBitmap[i][j].green < 0)
                    filteredBitmap[i][j].green = 0;
                if (filteredBitmap[i][j].blue < 0)
                    filteredBitmap[i][j].blue = 0;

            }
        }

        return filteredImage;
    }
}