package ru.nsu.fit.g15203.vorobyeva.filters;

import ru.nsu.fit.g15203.vorobyeva.MyImage;

public class SobelOperator implements FilterType {

    @Override
    public MyImage apply(MyImage image) {
        FilterType grey = new GreyShades();
        image = grey.apply(image);

        MyImage filteredImage = new MyImage(image);
        MyImage.ImageColor[][] bitmap = image.getBitMap();
        MyImage.ImageColor[][] filteredBitmap = filteredImage.getBitMap();

        for (int i = 1; i < filteredImage.getHeight() - 1; i++) {
            for (int j = 1; j < filteredImage.getWidth() - 1; j++) {

                int y = -bitmap[i - 1][j - 1].red -
                        2 * bitmap[i - 1][j].red -
                        bitmap[i - 1][j + 1].red +
                        bitmap[i + 1][j - 1].red +
                        2 * bitmap[i + 1][j].red +
                        bitmap[i + 1][j + 1].red;
                int x = -bitmap[i - 1][j - 1].red -
                        2 * bitmap[i][j - 1].red -
                        bitmap[i + 1][j - 1].red +
                        bitmap[i - 1][j + 1].red +
                        2 * bitmap[i][j + 1].red +
                        bitmap[i + 1][j + 1].red;

                int r = (int) Math.sqrt(x * x + y * y);

                if (r < 0)
                    r = 0;
                if (r > 255)
                    r = 255;

                filteredBitmap[i][j].red = r;
                filteredBitmap[i][j].green = r;
                filteredBitmap[i][j].blue = r;
            }
        }
        return filteredImage;
    }
}
