package ru.nsu.fit.g15203.vorobyeva.filters;


import ru.nsu.fit.g15203.vorobyeva.MyImage;

public class Sharpness implements FilterType {

    @Override
    public MyImage apply(MyImage image) {
        MyImage filteredImage = new MyImage(image);

        double[][] core = {{-1, -1, -1},
                {-1, 9, -1},
                {-1, -1, -1}};

        MyImage.ImageColor[][] bitmap = image.getBitMap();
        MyImage.ImageColor[][] filteredBitmap = filteredImage.getBitMap();

        for (int i = 1; i < filteredImage.getHeight() - 1; i++) {
            for (int j = 1; j < filteredImage.getWidth() - 1; j++) {
                filteredBitmap[i][j].red = (int) (core[0][0] * bitmap[i - 1][j - 1].red +
                        core[0][1] * bitmap[i - 1][j].red +
                        core[0][2] * bitmap[i - 1][j + 1].red +
                        core[1][0] * bitmap[i][j + 1].red +
                        core[1][1] * bitmap[i][j].red +
                        core[1][2] * bitmap[i][j + 1].red +
                        core[2][0] * bitmap[i + 1][j - 1].red +
                        core[2][1] * bitmap[i + 1][j].red +
                        core[2][2] * bitmap[i + 1][j + 1].red);

                filteredBitmap[i][j].green = (int) (core[0][0] * bitmap[i - 1][j - 1].green +
                        core[0][1] * bitmap[i - 1][j].green +
                        core[0][2] * bitmap[i - 1][j + 1].green +
                        core[1][0] * bitmap[i][j + 1].green +
                        core[1][1] * bitmap[i][j].green +
                        core[1][2] * bitmap[i][j + 1].green +
                        core[2][0] * bitmap[i + 1][j - 1].green +
                        core[2][1] * bitmap[i + 1][j].green +
                        core[2][2] * bitmap[i + 1][j + 1].green);

                filteredBitmap[i][j].blue = (int) (core[0][0] * bitmap[i - 1][j - 1].blue +
                        core[0][1] * bitmap[i - 1][j].blue +
                        core[0][2] * bitmap[i - 1][j + 1].blue +
                        core[1][0] * bitmap[i][j + 1].blue +
                        core[1][1] * bitmap[i][j].blue +
                        core[1][2] * bitmap[i][j + 1].blue +
                        core[2][0] * bitmap[i + 1][j - 1].blue +
                        core[2][1] * bitmap[i + 1][j].blue +
                        core[2][2] * bitmap[i + 1][j + 1].blue);

                if (filteredBitmap[i][j].red > 255)
                    filteredBitmap[i][j].red = 255;
                if (filteredBitmap[i][j].green > 255)
                    filteredBitmap[i][j].green = 255;
                if (filteredBitmap[i][j].blue > 255)
                    filteredBitmap[i][j].blue = 255;

                if (filteredBitmap[i][j].red < 0)
                    filteredBitmap[i][j].red = 0;
                if (filteredBitmap[i][j].green < 0)
                    filteredBitmap[i][j].green = 0;
                if (filteredBitmap[i][j].blue < 0)
                    filteredBitmap[i][j].blue = 0;
            }
        }

        return filteredImage;
    }
}
