package ru.nsu.fit.g15203.vorobyeva.filters;

import ru.nsu.fit.g15203.vorobyeva.MyImage;

public class GreyShades implements FilterType {
    @Override
    public MyImage apply(MyImage image) {
        MyImage filteredImage = new MyImage(image);
        MyImage.ImageColor[][] filteredBitmap = filteredImage.getBitMap();

        for (int i = 0; i < filteredImage.getHeight(); i++) {
            for (int j = 0; j < filteredImage.getWidth(); j++) {
                int r = filteredBitmap[i][j].red;
                int g = filteredBitmap[i][j].green;
                int b = filteredBitmap[i][j].blue;

                int y = (int)((0.299) * r +
                        (0.587) * g +
                        (0.114) * b);

                if (y < 0)
                    y = 0;

                if (y > 255)
                    y = 255;

                filteredBitmap[i][j].red = y;
                filteredBitmap[i][j].green = y;
                filteredBitmap[i][j].blue = y;
            }
        }

        return filteredImage;
    }
}
