package ru.nsu.fit.g15203.vorobyeva;

import ru.nsu.fit.g15203.vorobyeva.MyImage;
import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileSystem {
    private static final String DEFAULT_FILENAME = "new.bmp";
    private static final String DIRECTORY_SAVE = "FIT_15203_Vorobyeva_Lika_Filter_Data/";
    public boolean saved = false;
    public String currentFilename = null;

    private JFileChooser fileChooser = new JFileChooser();
    JFrame jFrame;

    public FileSystem(JFrame jFrame) {
        this.jFrame = jFrame;

        fileChooser.setCurrentDirectory(new File(DIRECTORY_SAVE));
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setFileFilter(new FileNameExtensionFilter("BMP/PNG Images",
                "bmp", "png"));
    }

    public boolean saveState(MainPanel panel) {
        File file = new File(DEFAULT_FILENAME);
        String fileName;

        fileChooser.setSelectedFile(file);
        try {
            if (!saved) {
                int ret = fileChooser.showSaveDialog(jFrame);

                if (ret == JFileChooser.APPROVE_OPTION) {
                    file = fileChooser.getSelectedFile();
                    fileName = file.getAbsolutePath();
                    Path path = Paths.get(fileName);

                    if (!path.toString().endsWith(".bmp") || !path.toString().endsWith(".png")) {
                        file = new File(fileName + ".bmp");
                    }
                    panel.zoneCPanel.getImage().write(new FileOutputStream(fileName));

                    currentFilename = file.getAbsolutePath();
                } else {
                    return false;
                }
            } else {
                file = new File(currentFilename);
                panel.zoneCPanel.getImage().write(new FileOutputStream(file));
            }
            saved = true;

            return true;
        }
        catch (FileNotFoundException f){
            JOptionPane.showMessageDialog(jFrame,  "File not found");
        }
        catch (IOException e1) {
            JOptionPane.showMessageDialog(jFrame, "Unsupported file");
        }

        return false;
    }

    public void loadFromFile(MainPanel panel) {
        File file = new File(DEFAULT_FILENAME);

        fileChooser.setSelectedFile(file);

        int ret = fileChooser.showOpenDialog(jFrame);

        if (ret == JFileChooser.APPROVE_OPTION) {
            panel.reset();
            file = fileChooser.getSelectedFile();
            MyImage image = new MyImage();
            try {
                if (file.toString().endsWith(".png")) {
                    BufferedImage bufferedImage = ImageIO.read(file);
                    File temp = new File("temp.bmp");
                    ImageIO.write(bufferedImage, "BMP", temp);
                    image.read(new FileInputStream(temp));
                    panel.setImage(image);
                }
                else {
                    image.read(new FileInputStream(file));
                    panel.setImage(image);
                }
            } catch (IOException e) {
                JOptionPane.showMessageDialog(jFrame,"Unsupported file");
            }
            currentFilename = file.getAbsolutePath();
        } else {
            return;
        }
        saved = true;
    }
}
