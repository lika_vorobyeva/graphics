package ru.nsu.fit.g15203.vorobyeva;

import ru.nsu.fit.g15203.vorobyeva.actions.*;
import ru.nsu.fit.g15203.vorobyeva.actions.applyFilters.*;
import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.*;



public class MainFrame {
    private JFrame jFrame = new JFrame();
    private JScrollPane scrollPane;
    private MainPanel mainPanel;
    private JToolBar toolBar;
    private FileSystem fileSystem;

    protected Action newAction, openAction, saveAction, saveAsAction,
            copyLeftAction, copyRightAction, selectAction,
            watercolorAction, monochromeAction, doubleSizeAction,
            floydSteinbergAction, orderedDitheringAction, gaussBlurAction,
            sharpnessAction, greyShadeAction, negativeAction,  gammaAction,
            robertsOperatorAction, sobelOperatorAction, stampAction,
            rotationAction, aboutAction, exitAction;

    public MainFrame() {
        fileSystem = new FileSystem(jFrame);

        mainPanel = new MainPanel();
        mainPanel.setPreferredSize(new Dimension(1130, 370));

        scrollPane = new JScrollPane(mainPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        initActions(mainPanel);
        createFileMenu();
        createToolBar();
    }

    public void initActions(MainPanel panel) {
        //file menu
        newAction = new NewAction("New File",
                createToolIcon("new"),
                "new file",
                panel, fileSystem, jFrame);
        openAction = new OpenAction("Open",
                createToolIcon("open"),
                "open image",
                panel, fileSystem);
        saveAction = new SaveAction("Save",
                createToolIcon("save"),
                "save image",
                panel, fileSystem);
        saveAsAction = new SaveAsAction("Save As",
                "save image as new file",
                panel, fileSystem);
        exitAction = new ExitAction("Exit", "Exit",
                mainPanel, jFrame, fileSystem);


        copyLeftAction = new CopyLeftAction("Copy from C to B",
                createToolIcon("left"),
                "copy from C to B", panel, jFrame);
        copyRightAction = new CopyRightAction("Copy from B to C",
                createToolIcon("right"),
                "copy from B to C", panel, jFrame);
        selectAction = new SelectAction("Select",
                createToolIcon("select"),
                "select", panel, jFrame);

        //filters
        greyShadeAction = new GreyShadeAction("GreyShades",
                createToolIcon("grey"),
                "grey shades", panel, jFrame);
        negativeAction = new NegativeAction("Negative",
                createToolIcon("negative"),
                "negative filter", panel, jFrame);
        //dithering
        floydSteinbergAction = new FloydSteinbergAction("Floyd Steinberg",
                createToolIcon("floyd"),
                "Floyd Steinberg filter", panel, jFrame);
        orderedDitheringAction = new OrderedDitheringAction("Ordered dithering",
                createToolIcon("dith"),
                "ordered dithering filter", panel, jFrame);

        doubleSizeAction = new DoubleSizeAction("Double size",
                createToolIcon("double"),
                "double size image", panel, jFrame);
        //diff
        robertsOperatorAction = new RobertsOperatorAction("Roberts cross",
                createToolIcon("robert"),
                "Roberts cross", panel, jFrame);
        sobelOperatorAction = new SobelOperatorAction("Sobel operator",
                createToolIcon("sobel"),
                "Sobel operator", panel, jFrame);

        watercolorAction = new WatercolorAction("Watercolor",
                createToolIcon("watercolor"),
                "watercolor filter", panel, jFrame);

        monochromeAction = new MonochromeAction("Monochrome",
                createToolIcon("mono"),
                "monochrome filter", panel, jFrame);


        gaussBlurAction = new GaussBlurAction("Gauss blur",
                createToolIcon("blur"),
                "Gauss blur filter", panel, jFrame);

        sharpnessAction = new SharpnessAction("Sharpness",
                createToolIcon("sharp"),
                "sharpness filter", panel, jFrame);

        stampAction = new StampAction("Stamp filter",
                createToolIcon("stamp"),
                "stamp filter", panel, jFrame);

        gammaAction = new GammaAction("Gamma",
                createToolIcon("gamma"),
                "gamma correction", panel, jFrame);

        rotationAction = new RotationAction("Rotation",
                createToolIcon("rotate"),
                "rotation to input angle", panel, jFrame);

        //help
        aboutAction = new AboutAction("About",
                createToolIcon("about"),
                "about", panel);
    }

    private static ImageIcon createToolIcon(String imageName) {
        String icon = "src/resources/"
                + imageName
                + ".png";

        return new ImageIcon(icon);
    }

    public JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        menuBar.add(createFileMenu());
        menuBar.add(createFiltersMenu());
        menuBar.add(createHelpMenu());

        return menuBar;
    }

    public JMenu createFileMenu() {
        JMenu fileMenu = new JMenu("File");
        JMenuItem jmiNF = new JMenuItem(newAction);
        jmiNF.setIcon(null);
        fileMenu.add(jmiNF);

        JMenuItem jmOpen = new JMenuItem(openAction);
        jmOpen.setIcon(null);
        fileMenu.add(jmOpen);

        JMenuItem jmSave = new JMenuItem(saveAction);
        jmSave.setIcon(null);
        fileMenu.add(jmSave);

        JMenuItem jmSaveAs = new JMenuItem(saveAsAction);
        fileMenu.add(jmSaveAs);
        fileMenu.addSeparator();

        JMenuItem jmiSelect = new JMenuItem(selectAction);
        jmiSelect.setIcon(null);
        fileMenu.add(jmiSelect);

        JMenuItem jmiRight = new JMenuItem(copyRightAction);
        jmiRight.setIcon(null);
        fileMenu.add(jmiRight);

        JMenuItem jmiLeft = new JMenuItem(copyLeftAction);
        jmiLeft.setIcon(null);
        fileMenu.add(jmiLeft);

        fileMenu.addSeparator();
        JMenuItem jmExit = new JMenuItem(exitAction);
        fileMenu.add(jmExit);

        return fileMenu;
    }

    public JMenu createFiltersMenu() {
        JMenu filtersMenu = new JMenu("Filters");

        JMenuItem jmiZoom = new JMenuItem(doubleSizeAction);
        jmiZoom.setIcon(null);
        filtersMenu.add(jmiZoom);

        JMenuItem jmiStamp = new JMenuItem(stampAction);
        jmiStamp.setIcon(null);
        filtersMenu.add(jmiStamp);

        JMenu jmCanal = new JMenu("Canal");
        filtersMenu.add(jmCanal);
        JMenuItem jmiBL = new JMenuItem(greyShadeAction);
        jmiBL.setIcon(null);
        jmCanal.add(jmiBL);
        JMenuItem jmiNegative = new JMenuItem(negativeAction);
        jmiNegative.setIcon(null);
        jmCanal.add(jmiNegative);
        //TODO
        JMenuItem jmiGamma = new JMenuItem(gammaAction);
        jmiGamma.setIcon(null);
        jmCanal.add(jmiGamma);

        JMenu jmMatrix = new JMenu("Matrix");
        filtersMenu.add(jmMatrix);
        JMenuItem jmiBlur = new JMenuItem(gaussBlurAction);
        jmiBlur.setIcon(null);
        jmMatrix.add(jmiBlur);
        JMenuItem jmiSharpness = new JMenuItem(sharpnessAction);
        jmiSharpness.setIcon(null);
        jmMatrix.add(jmiSharpness);
        JMenuItem jmiAquarelle = new JMenuItem(watercolorAction);
        jmiAquarelle.setIcon(null);
        jmMatrix.add(jmiAquarelle);


        JMenu jmDithering = new JMenu("Dithering");
        filtersMenu.add(jmDithering);
        JMenuItem jmiOrdered = new JMenuItem(orderedDitheringAction);
        jmiOrdered.setIcon(null);
        jmDithering.add(jmiOrdered);
        JMenuItem jmiFloyd = new JMenuItem(floydSteinbergAction);
        jmiFloyd.setIcon(null);
        jmDithering.add(jmiFloyd);

        JMenu jmEdges = new JMenu("Edges");
        filtersMenu.add(jmEdges);
        JMenuItem jmiRE = new JMenuItem(robertsOperatorAction);
        jmiRE.setIcon(null);
        jmEdges.add(jmiRE);
        JMenuItem jmiSE = new JMenuItem(sobelOperatorAction);
        jmiSE.setIcon(null);
        jmEdges.add(jmiSE);

        return filtersMenu;
    }

    public JMenu createHelpMenu() {
        JMenu helpMenu = new JMenu("Help");

        JMenuItem jmiAbout = new JMenuItem(aboutAction);
        helpMenu.add(jmiAbout);

        return helpMenu;
    }

    public void createToolBar() {
        MatteBorder matteBorder = new MatteBorder(1, 0, 0, 0, Color.BLACK);
        JButton button;

        toolBar = new JToolBar();
        toolBar.setBorder(matteBorder);
        toolBar.setRollover(true);

        Action[] actions = {newAction, openAction, saveAction,
                copyLeftAction, copyRightAction, selectAction,
                watercolorAction, monochromeAction, doubleSizeAction,
                floydSteinbergAction, orderedDitheringAction, gaussBlurAction,
                sharpnessAction, greyShadeAction, negativeAction,  gammaAction,
                robertsOperatorAction, sobelOperatorAction, stampAction, rotationAction};

        for (int i = 0; i < actions.length; i++){
            button = new JButton(actions[i]);
            button.setText("");

            if (i == 3)
                toolBar.addSeparator();
            toolBar.add(button);
        }
        button = new JButton(aboutAction);
        button.setText("");

        toolBar.addSeparator();
        toolBar.add(button);

    }

    public void init() {
        jFrame.setPreferredSize(new Dimension(1160, 370));
        jFrame.setMinimumSize(new Dimension(800, 600));
        jFrame.setLayout(new BorderLayout());
        jFrame.setJMenuBar(createMenuBar());

        jFrame.add(scrollPane, BorderLayout.CENTER);
        jFrame.add(toolBar, BorderLayout.NORTH);

        jFrame.setTitle("Filter");
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        WindowListener winListener = new MyWindowsListener();
        jFrame.addWindowListener(winListener);
        jFrame.pack();
        jFrame.setVisible(true);
        jFrame.setResizable(true);
        jFrame.setLocationRelativeTo(null);
    }

    private class MyWindowsListener implements WindowListener {
        @Override
        public void windowOpened(WindowEvent e) {
        }

        //TODO comporator
        @Override
        public void windowClosing(WindowEvent e) {
            if(mainPanel.zoneCPanel.getImage() != null) {
                int result = JOptionPane.showConfirmDialog(jFrame,
                        "Do you want to save filtered image?",
                        "Exit",
                        JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.YES_OPTION) {
                    fileSystem.saveState(mainPanel);
                }
            }
        }

        @Override
        public void windowClosed(WindowEvent e) {

        }

        @Override
        public void windowIconified(WindowEvent e) {

        }

        @Override
        public void windowDeiconified(WindowEvent e) {

        }

        @Override
        public void windowActivated(WindowEvent e) {

        }

        @Override
        public void windowDeactivated(WindowEvent e) {

        }
    }
}