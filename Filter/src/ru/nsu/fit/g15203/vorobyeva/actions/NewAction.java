package ru.nsu.fit.g15203.vorobyeva.actions;

import ru.nsu.fit.g15203.vorobyeva.FileSystem;
import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;


public class NewAction extends AbstractAction{
    private MainPanel panel;
    private FileSystem fileSystem;
    private JFrame jFrame;

    public NewAction(String text, ImageIcon icon,
                      String desc, MainPanel panel, FileSystem fileSystem, JFrame jFrame) {
        super(text, icon);
        putValue(SHORT_DESCRIPTION, desc);

        this.panel = panel;
        this.fileSystem = fileSystem;
        this.jFrame = jFrame;
    }

    public void actionPerformed(ActionEvent e) {
        if (panel.zoneCPanel.getImage() != null) {
            int result = JOptionPane.showConfirmDialog(jFrame,
                    "Do you want to save the progress?",
                    "New",
                    JOptionPane.YES_NO_CANCEL_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                fileSystem.saveState(panel);
            }
            if (result != JOptionPane.CANCEL_OPTION) {
                panel.reset();

                panel.enabled = false;
                fileSystem.saved = false;
                panel.imageLoaded = false;

            }
        }
        else {
            panel.reset();

            panel.enabled = false;
            fileSystem.saved = false;
            panel.imageLoaded = false;
        }
    }
}
