package ru.nsu.fit.g15203.vorobyeva.actions.applyFilters;

import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;
import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class OrderedDitheringAction extends AbstractAction {
    private MainPanel panel;
    private JFrame jFrame;

    public OrderedDitheringAction(String text, ImageIcon icon,
                                  String desc, MainPanel panel, JFrame jFrame) {
        super(text, icon);
        putValue(SHORT_DESCRIPTION, desc);
        this.panel = panel;
        this.jFrame = jFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!panel.isImageLoaded()) {
            JOptionPane.showMessageDialog(jFrame, "Nothing to filter!",
                    "Panel B is clear", JOptionPane.INFORMATION_MESSAGE);
        }
        else
            new Thread(() -> panel.orderedDithering()).start();
    }
}
