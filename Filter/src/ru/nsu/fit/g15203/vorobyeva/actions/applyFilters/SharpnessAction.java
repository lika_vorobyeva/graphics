package ru.nsu.fit.g15203.vorobyeva.actions.applyFilters;

import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;


public class SharpnessAction extends AbstractAction {
    private MainPanel panel;
    private JFrame jFrame;

    public SharpnessAction(String text, ImageIcon icon,
                           String desc, MainPanel panel, JFrame jFrame) {
        super(text, icon);
        putValue(SHORT_DESCRIPTION, desc);
        this.panel = panel;
        this.jFrame = jFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (panel.isImageLoaded())
            new Thread(() -> panel.sharpness()).start();
        else
            JOptionPane.showMessageDialog(jFrame, "Nothing to filter!",
                    "Panel B is clear", JOptionPane.INFORMATION_MESSAGE);
    }
}