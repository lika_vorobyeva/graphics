package ru.nsu.fit.g15203.vorobyeva.actions.applyFilters;

import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GammaAction extends AbstractAction {
        private MainPanel panel;
        private  JFrame jFrame;

        public GammaAction(String text, ImageIcon icon,
                           String desc, MainPanel panel, JFrame jFrame) {
            super(text, icon);
            putValue(SHORT_DESCRIPTION, desc);
            this.panel = panel;
            this.jFrame = jFrame;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (!panel.isImageLoaded()) {
                JOptionPane.showMessageDialog(jFrame, "Nothing to filter!",
                        "Panel B is clear", JOptionPane.INFORMATION_MESSAGE);
                return;
            }

            panel.gamma(2);

            JDialog dialog = new JDialog();

            JPanel settingPanel = new JPanel(new GridLayout(2, 1));
            settingPanel.setBorder(BorderFactory.createTitledBorder("gamma gradation"));
            settingPanel.setPreferredSize(new Dimension(500, 50));

            final JSlider slider = new JSlider(JSlider.HORIZONTAL, 1, 20, 1);

            slider.setPreferredSize(new Dimension(400,50));
            slider.setMajorTickSpacing(1);
            slider.setPaintTicks(true);
            slider.setPaintLabels(true);

            JPanel gammaPanel = new JPanel();
            gammaPanel.setBorder(BorderFactory.createTitledBorder("gamma"));
            gammaPanel.add(slider);
            settingPanel.add(gammaPanel);

            final JButton saveButton = new JButton("Save");
            final JButton cancelButton = new JButton("Cancel");

            saveButton.addActionListener(s ->  dialog.dispose());
            cancelButton.addActionListener(c -> {
                panel.cancel();
                dialog.dispose();
            });

            dialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    super.windowClosing(e);
                    panel.cancel();
                }
            });

            slider.addChangeListener(change ->
                new Thread(() -> panel.gamma( slider.getValue())).start());

            JPanel buttonPanel = new JPanel();
            buttonPanel.add(saveButton);
            buttonPanel.add(cancelButton);
            settingPanel.add(buttonPanel);

            dialog.add(settingPanel);
            dialog.setPreferredSize(new Dimension(500,250));
            dialog.setResizable(true);
            dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            dialog.pack();
            dialog.setLocationRelativeTo(jFrame);
            dialog.setVisible(true);
        }
}

