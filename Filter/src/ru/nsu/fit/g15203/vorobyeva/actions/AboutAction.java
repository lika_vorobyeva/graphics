package ru.nsu.fit.g15203.vorobyeva.actions;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class AboutAction extends AbstractAction {
    private JPanel panel;

    public AboutAction(String text, ImageIcon icon,
                       String desc, JPanel panel) {
        super(text, icon);
        putValue(SHORT_DESCRIPTION, desc);
        this.panel = panel;
    }

    public void actionPerformed(ActionEvent e) {//TODO to frame
        JOptionPane.showMessageDialog(panel, "FIT_15203_Vorobyeva_Lika_Filter\n"+
                        "Author:   Vorobyeva Lika\n"+
                        "Year:  2018\n",
                "About", JOptionPane.INFORMATION_MESSAGE);
    }
}