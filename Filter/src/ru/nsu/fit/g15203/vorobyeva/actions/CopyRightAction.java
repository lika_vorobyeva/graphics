package ru.nsu.fit.g15203.vorobyeva.actions;

import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class CopyRightAction extends AbstractAction {
    private MainPanel panel;
    private JFrame jFrame;

    public CopyRightAction(String text, ImageIcon icon,
                          String desc, MainPanel panel, JFrame jFrame) {
        super(text, icon);
        putValue(SHORT_DESCRIPTION, desc);
        this.panel = panel;
        this.jFrame = jFrame;
    }

    public void actionPerformed(ActionEvent e) {
        if (panel.isImageLoaded())
            panel.copyFromB();
        else
            JOptionPane.showMessageDialog(jFrame, "Nothing to copy!",
                    "Panel B is clear", JOptionPane.INFORMATION_MESSAGE);
    }
}
