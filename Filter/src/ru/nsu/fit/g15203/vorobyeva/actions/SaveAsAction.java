package ru.nsu.fit.g15203.vorobyeva.actions;

import ru.nsu.fit.g15203.vorobyeva.FileSystem;
import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class SaveAsAction extends AbstractAction {
    private MainPanel panel;
    private FileSystem fileSystem;

    public SaveAsAction(String text, String desc,
                        MainPanel panel, FileSystem fileSystem) {
        super(text);
        putValue(SHORT_DESCRIPTION, desc);

        this.panel = panel;
        this.fileSystem = fileSystem;
    }

    public void actionPerformed(ActionEvent e) {
        if (panel.zoneCPanel.getImage() != null) {
            fileSystem.saved = false;
            fileSystem.saveState(panel);
        }
        else
            JOptionPane.showMessageDialog(panel, "Nothing to save!",
                    "Panel C is clear", JOptionPane.INFORMATION_MESSAGE);
    }
}