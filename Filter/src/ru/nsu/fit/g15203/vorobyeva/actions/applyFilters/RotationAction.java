package ru.nsu.fit.g15203.vorobyeva.actions.applyFilters;

import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class RotationAction extends AbstractAction {
    private MainPanel panel;
    private JFrame jFrame;

    public RotationAction(String text, ImageIcon icon,
            String desc, MainPanel panel, JFrame jFrame) {
        super(text, icon);
        putValue(SHORT_DESCRIPTION, desc);
        this.panel = panel;
        this.jFrame = jFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!panel.isImageLoaded()) {
            JOptionPane.showMessageDialog(jFrame, "Nothing to rotate!",
                    "Panel B is clear", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        DigitFilter digitFilter = new DigitFilter();

        final JDialog dialog = new JDialog();
        JPanel settingPanel = new JPanel(new GridLayout(2, 1));
        settingPanel.setBorder(BorderFactory.createTitledBorder("angle"));

        JLabel angle = new JLabel("Enter value", SwingConstants.CENTER);
        JTextField angleField = new JTextField();
        PlainDocument doc = (PlainDocument) angleField.getDocument();
        doc.setDocumentFilter(digitFilter);

        settingPanel.add(angle);
        settingPanel.add(angleField);

        final JButton saveButton = new JButton("Save");
        final JButton cancelButton = new JButton("Cancel");

        saveButton.addActionListener(s -> {
            if (!angleField.getText().isEmpty()) {
                panel.zoneCPanel.setImage(null);
                new Thread(() -> panel.rotation(Integer.parseInt(angleField.getText()), panel)).start();
            }
            dialog.dispose();
        });
        cancelButton.addActionListener(c -> {
            panel.cancel();
            dialog.dispose();
        });

        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                panel.cancel();
            }
        });


        JPanel buttonPanel = new JPanel();
        buttonPanel.add(saveButton);
        buttonPanel.add(cancelButton);
        settingPanel.add(buttonPanel);

        dialog.add(settingPanel);
        dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        dialog.setModal(true);
        dialog.pack();
        dialog.setLocationRelativeTo(jFrame);
        dialog.setVisible(true);
    }

    class DigitFilter extends DocumentFilter {
        private static final String DIGITS = "\\d+";

        @Override
        public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
            if (string.matches(DIGITS)) {
                super.insertString(fb, offset, string, attr);
            }
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String string, AttributeSet attrs) throws BadLocationException {
            if (string.matches(DIGITS)) {
                super.replace(fb, offset, length, string, attrs);
            }
        }
    }
}
