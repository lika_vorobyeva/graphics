package ru.nsu.fit.g15203.vorobyeva.actions;

import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;
import javax.swing.*;
import java.awt.event.ActionEvent;

public class SelectAction extends AbstractAction {
    private MainPanel panel;
    private JFrame jFrame;

    public SelectAction(String text, ImageIcon icon,
                      String desc, MainPanel panel, JFrame jFrame) {
        super(text, icon);
        putValue(SHORT_DESCRIPTION, desc);
        this.panel = panel;
        this.jFrame = jFrame;
    }

    public void actionPerformed(ActionEvent e) {
        if (panel.zoneAPanel.getImage() != null)
            panel.enabled = !panel.enabled;
        else {
            panel.enabled = false;
            JOptionPane.showMessageDialog(jFrame, "Nothing to select!",
                    "Panel A is clear", JOptionPane.INFORMATION_MESSAGE);
        }
        if (!panel.enabled)
            panel.select.setVisible(false);
    }
}
