package ru.nsu.fit.g15203.vorobyeva.actions;

import ru.nsu.fit.g15203.vorobyeva.FileSystem;
import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ExitAction extends AbstractAction {
    private MainPanel mainPanel;
    private JFrame jFrame;
    private FileSystem fileSystem;

    public ExitAction(String text, String desc,
                      MainPanel mainPanel, JFrame jFrame, FileSystem fileSystem) {
        super(text);
        putValue(SHORT_DESCRIPTION, desc);
        this.fileSystem = fileSystem;
        this.jFrame = jFrame;
        this.mainPanel = mainPanel;
    }

    public void actionPerformed(ActionEvent e) {
        if(mainPanel.zoneCPanel.getImage() != null) {
            int result = JOptionPane.showConfirmDialog(jFrame,
                    "Do you want to save filtered image?",
                    "Exit",
                    JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                fileSystem.saveState(mainPanel);
            }
        }
        System.exit(0);
    }
}
