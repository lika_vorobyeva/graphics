package ru.nsu.fit.g15203.vorobyeva.actions.applyFilters;

import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class FloydSteinbergAction extends AbstractAction{
    private MainPanel panel;
    private JFrame jFrame;

    public FloydSteinbergAction(String text, ImageIcon icon,
                                String desc, MainPanel panel, JFrame jFrame) {
        super(text, icon);
        putValue(SHORT_DESCRIPTION, desc);
        this.panel = panel;
        this.jFrame = jFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!panel.isImageLoaded()) {
            JOptionPane.showMessageDialog(jFrame, "Nothing to filter!",
                    "Panel B is clear", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        panel.floydSteinberg(2, 2, 2);

        final JDialog dialog = new JDialog();
        JPanel settingPanel = new JPanel(new GridLayout(4, 1));
        settingPanel.setBorder(BorderFactory.createTitledBorder("color channel gradation"));

        final JSlider rSlider = new JSlider(JSlider.HORIZONTAL, 2, 256, 2);
        rSlider.setMajorTickSpacing(100);
        rSlider.setPaintTicks(true);
        rSlider.setPaintLabels(true);

        JPanel redPanel = new JPanel();
        redPanel.setBorder(BorderFactory.createTitledBorder("red"));
        redPanel.add(rSlider);
        settingPanel.add(redPanel);

        final JSlider gSlider = new JSlider(JSlider.HORIZONTAL, 2, 256, 2);
        gSlider.setMajorTickSpacing(100);
        gSlider.setPaintTicks(true);
        gSlider.setPaintLabels(true);

        JPanel greenPanel = new JPanel();
        greenPanel.setBorder(BorderFactory.createTitledBorder("green"));
        greenPanel.add(gSlider);
        settingPanel.add(greenPanel);

        final JSlider bSlider = new JSlider(JSlider.HORIZONTAL, 2, 256, 2);
        bSlider.setMajorTickSpacing(100);
        bSlider.setPaintTicks(true);
        bSlider.setPaintLabels(true);

        JPanel bluePanel = new JPanel();
        bluePanel.setBorder(BorderFactory.createTitledBorder("blue"));
        bluePanel.add(bSlider);
        settingPanel.add(bluePanel);

        final JButton saveButton = new JButton("Save");
        final JButton cancelButton = new JButton("Cancel");

        saveButton.addActionListener(s ->  dialog.dispose());
        cancelButton.addActionListener(c -> {
            panel.cancel();
            dialog.dispose();
        });

        ChangeListener changeListener = change ->
            new Thread(() -> panel.floydSteinberg
                    (rSlider.getValue(), gSlider.getValue(), bSlider.getValue())).start();


        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                panel.cancel();
            }
        });

        rSlider.addChangeListener(changeListener);
        gSlider.addChangeListener(changeListener);
        bSlider.addChangeListener(changeListener);

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(saveButton);
        buttonPanel.add(cancelButton);
        settingPanel.add(buttonPanel);

        dialog.add(settingPanel);
        dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        dialog.setModal(true);
        dialog.pack();
        dialog.setLocationRelativeTo(jFrame);
        dialog.setVisible(true);
    }
}
