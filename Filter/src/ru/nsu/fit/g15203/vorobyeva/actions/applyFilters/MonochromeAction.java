package ru.nsu.fit.g15203.vorobyeva.actions.applyFilters;

import ru.nsu.fit.g15203.vorobyeva.panels.MainPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MonochromeAction extends AbstractAction {
    private MainPanel panel;
    private JFrame jFrame;

    public MonochromeAction(String text, ImageIcon icon,
                            String desc, MainPanel panel, JFrame jFrame) {
        super(text, icon);
        putValue(SHORT_DESCRIPTION, desc);
        this.panel = panel;
        this.jFrame = jFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!panel.isImageLoaded()) {
            JOptionPane.showMessageDialog(jFrame, "Nothing to filter!",
                    "Panel B is clear", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        panel.monochrome(100);

        final JDialog dialog = new JDialog();
        JPanel settingPanel = new JPanel(new GridLayout(2, 1));
        JPanel buttonPanel = new JPanel();

        JPanel blackPanel = new JPanel();
        blackPanel.setBorder(BorderFactory.createTitledBorder("Blackness"));

        final JSlider hSlider = new JSlider(JSlider.HORIZONTAL, 0, 255, 100);
        hSlider.setMajorTickSpacing(100);
        hSlider.setMinorTickSpacing(20);
        hSlider.setPaintTicks(true);
        hSlider.setPaintLabels(true);
        blackPanel.add(hSlider);

        final JButton saveButton = new JButton("Save");
        final JButton cancelButton = new JButton("Cancel");

        saveButton.addActionListener(s ->  dialog.dispose());
        cancelButton.addActionListener(c -> {
            panel.cancel();
            dialog.dispose();
        });

        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                panel.cancel();
            }
        });
        hSlider.addChangeListener(change -> {
            int blackness = hSlider.getValue();
            new Thread(() -> panel.monochrome(blackness)).start();
        });

        settingPanel.add(blackPanel);
        settingPanel.add(buttonPanel);

        buttonPanel.add(saveButton);
        buttonPanel.add(cancelButton);

        dialog.add(settingPanel);
        dialog.pack();
        dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        dialog.setModal(true);
        dialog.setLocationRelativeTo(jFrame);
        dialog.setVisible(true);
    }
}
