package ru.nsu.fit.g15203.vorobyeva;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Hexagon {
    private static final int SIDES = 6;

    private Point center = new Point(0, 0);
    private int lineThickness;
    private int[] xpoints;
    private int[] ypoints;

    public boolean colored = false;

    public Hexagon(Point center, int radius, int lineThickness) {
        xpoints = new int[SIDES];
        ypoints = new int[SIDES];

        this.center = center;
        this.lineThickness = lineThickness;

        xpoints[0] = center.x;
        xpoints[1] = center.x + (int) Math.ceil(Math.sqrt(3) * radius / 2);
        xpoints[2] = center.x + (int) Math.ceil(Math.sqrt(3) * radius / 2);
        xpoints[3] = center.x;
        xpoints[4] = center.x - (int) Math.ceil(Math.sqrt(3) * radius / 2);
        xpoints[5] = center.x - (int) Math.ceil(Math.sqrt(3) * radius / 2);

        ypoints[0] = center.y - radius;
        ypoints[1] = center.y - radius / 2;
        ypoints[2] = center.y + radius / 2;
        ypoints[3] = center.y + radius;
        ypoints[4] = center.y + radius / 2;
        ypoints[5] = center.y - radius / 2;
    }

    public Point getCenter() {
        return center;
    }

    public void draw(Graphics g, int radius) {
        if (lineThickness > 1) {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setStroke(new BasicStroke(lineThickness));
            g2d.setColor(Color.BLACK);
            g2d.drawPolygon(xpoints, ypoints, SIDES);

            Hexagon innerHex = new Hexagon(this.center, radius, lineThickness);
            g2d.setColor(Color.LIGHT_GRAY);
            g2d.drawPolygon(innerHex.xpoints, innerHex.ypoints, SIDES);
        }

        else {
            g.setColor(Color.BLACK);
            DrawBL bl = new DrawBL();
            bl.drawBLHex(xpoints, ypoints, g);
            g.setColor(Color.WHITE);
        }
    }

    public void fill(Graphics g, Color hexColor, BufferedImage bufferedImage){
        Span span = new Span(center, center);
        span.fill(center, hexColor, bufferedImage);
        g.setColor(Color.WHITE);
    }

    public boolean isHex(Point p, int radius){
        Hexagon innerHex = new Hexagon(this.center, radius, lineThickness);
        if(p.x <= innerHex.xpoints[1] && p.x >= innerHex.xpoints[5] && p.y >= innerHex.ypoints[1] && p.y <= innerHex.ypoints[2]) {
            return true;
        }

        int a0 = (innerHex.xpoints[0] - p.x) * (innerHex.ypoints[1] - innerHex.ypoints[0])
                - (innerHex.xpoints[1] - innerHex.xpoints[0]) * (innerHex.ypoints[0] - p.y);
        int b0 = (innerHex.xpoints[1] - p.x) * (innerHex.ypoints[5] - innerHex.ypoints[1])
                - (innerHex.xpoints[5] - innerHex.xpoints[1]) * (innerHex.ypoints[1] - p.y);
        int c0 = (innerHex.xpoints[5] - p.x) * (innerHex.ypoints[0] - innerHex.ypoints[5])
                - (innerHex.xpoints[0] - innerHex.xpoints[5]) * (innerHex.ypoints[5] - p.y);

        if ((a0 >= 0 && b0 >= 0 && c0 >= 0) || (a0 <= 0 && b0 <= 0 && c0 <= 0)) {
            return true;
        }

        int a1 = (innerHex.xpoints[3] - p.x) * (innerHex.ypoints[2] - innerHex.ypoints[3])
                - (innerHex.xpoints[2] - innerHex.xpoints[3]) * (innerHex.ypoints[3] - p.y);
        int b1 = (innerHex.xpoints[2] - p.x) * (innerHex.ypoints[4] - innerHex.ypoints[2])
                - (innerHex.xpoints[4] - innerHex.xpoints[2]) * (innerHex.ypoints[2] - p.y);
        int c1 = (innerHex.xpoints[4] - p.x) * (innerHex.ypoints[3] - innerHex.ypoints[4])
                - (innerHex.xpoints[3] - innerHex.xpoints[4]) * (innerHex.ypoints[4] - p.y);

        if ((a1 >= 0 && b1 >= 0 && c1 >= 0) || (a1 <= 0 && b1 <= 0 && c1 <= 0)) {
            return true;
        }

        return false;
    }
}