package ru.nsu.fit.g15203.vorobyeva;

import javax.swing.*;
import java.awt.*;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.util.*;
import java.util.Timer;


public class HexGrid extends JPanel {

    public int WIDTH = 800;
    public int HEIGHT = 600;

    private Life life;
    private Graphics g;
    private BufferedImage bufferedImage;
    private Timer timer = new Timer();
    private TimerTask timerTask = new scheduler();
    private String string;

    private int lineThickness;
    private int innerRadius;
    private int radius;
    private int numberOfColumns;
    private int numberOfLines;

    public boolean isPlay = false;
    public boolean showImpacts = false;
    public boolean XOR = false;

    private Point mouseClicked = new Point(-2,-2);
    private Point xor = new Point(0,0);
    private Hexagon[][] hexagons;

    private int deltaX;
    private int deltaXY;
    private int x;
    private int startY;
    private int deltaY;

    public HexGrid(int numberOfColumns, int numberOfLines, int radius, int lineThickness, Life life) {
        this.numberOfColumns = numberOfColumns;
        this.numberOfLines = numberOfLines;
        this.lineThickness = lineThickness;
        this.innerRadius = radius;

        resetParams(numberOfColumns, numberOfLines, radius, lineThickness);

        WIDTH = numberOfColumns * deltaX + 30;
        HEIGHT = (numberOfLines + 1) * deltaY;

        this.life = life;
        hexagons = new Hexagon[numberOfLines][numberOfColumns];

        bufferedImage = new BufferedImage(WIDTH, HEIGHT, bufferedImage.TYPE_INT_ARGB);
        g = bufferedImage.createGraphics();

        JLabel jLabel = new JLabel(new ImageIcon(bufferedImage));
        CustomListener customListener = new CustomListener();

        this.add(jLabel);
        jLabel.addMouseListener(customListener);
        jLabel.addMouseMotionListener(customListener);

        resetGrid();
    }

    public void nextStep(){
        life.nextStep();
        refillHexGrid();

        repaint();
    }

    public void resetGrid(){
        makeHexGrid();
        refillHexGrid();

        repaint();
    }

    public void play() {
        if (isPlay) {
            stop();
        } else {
             if (life.isTriggered()) {
                isPlay = true;
                timer.schedule(timerTask, 0, 500);
            }
        }
    }

    public void stop(){
        if(isPlay) {
            timer.cancel();
            timer.purge();

            timer = new java.util.Timer();
            timerTask = new scheduler();

            isPlay = false;
        }
    }

    public void makeHexGrid() {

        int y = startY;
        int length = numberOfColumns - 1;
        int originX;

        Point origin;

        for (int i = 0; i < numberOfLines; i++) {
            if ( i % 2 == 0 ) {
                length++;
                originX = x;
            } else {
                length--;
                originX = x + deltaXY;
            }
            origin = new Point(originX, y);

            for (int j = 0; j < length; j++) {
                hexagons[i][j] = new Hexagon(origin, radius, lineThickness);
                hexagons[i][j].draw(g, innerRadius);

                originX += deltaX;
                origin = new Point(originX, y);
            }
            y += deltaY;
        }
    }

    public void refillHexGrid(){
        int length = numberOfColumns - 1;

        for (int i = 0; i < numberOfLines; i++) {
            if (i % 2 == 0) {
                length++;
            } else {
                length--;
            }
            for (int j = 0; j < length; j++) {
                fill(i, j);
            }
        }
        repaint();
    }

    private void fill(int i, int j){
        if (life.isLiving[i][j]) {
            hexagons[i][j].fill(g, Color.GREEN, bufferedImage);
        } else {
            hexagons[i][j].fill(g, Color.LIGHT_GRAY, bufferedImage);
        }

        if (showImpacts && innerRadius > 11) {
            if (life.impact[i][j] % 1 == 0.00) {
                string = String.format("%.0f", life.impact[i][j]).replace(",", ".");
            } else {
                string = String.format("%.1f", life.impact[i][j]).replace(",", ".");
            }

            drawCenteredString(string, hexagons[i][j].getCenter().x,
                    hexagons[i][j].getCenter().y);
        }
    }

    public void fillImpc(Point center){
        fill(center.x, center.y);
        ArrayList<Point> first = firstImpc(center.x, center.y);
        ArrayList<Point> second = secondImpc(center.x, center.y);

        for (int i = 0; i < first.size(); i++) {
            Point point = first.get(i);
            if (first.get(i) == null) {
                break;
            }
            else
                fill(point.x, point.y);
        }
        for (int i = 0; i < second.size(); i++) {
            Point point = second.get(i);
            if (second.get(i) == null) {
                break;
            }
            else
                fill(point.x, point.y);
        }
    }

    private ArrayList<Point> firstImpc(int i, int j) {
        boolean top = i > 0;
        boolean down = i < numberOfLines - 1;
        boolean left = j > 0;
        boolean right = j < numberOfColumns - 1 - 1;
        boolean parity = i % 2 == 0;
        ArrayList<Point> arrayList = new ArrayList<>();


        if (right) {
            arrayList.add(new Point(i, j + 1));
        }
        if (left) {
            arrayList.add(new Point(i, j - 1));
        }

        if (top){
            if (parity) {
                if (left) {
                    arrayList.add(new Point(i - 1, j - 1));
                }
                if (right) {
                    arrayList.add(new Point(i - 1, j));
                }
            }
            else {
                arrayList.add(new Point(i - 1, j + 1));
                arrayList.add(new Point(i - 1, j));
            }
        }

        if (down) {
            if (parity) {
                if (left) {
                    arrayList.add(new Point(i + 1, j - 1));
                }
                if (right) {
                    arrayList.add(new Point(i + 1, j));
                }
            }

            else  {
                arrayList.add(new Point(i + 1, j));
                arrayList.add(new Point(i + 1, j + 1));
            }
        }

        return arrayList;
    }

    private ArrayList<Point> secondImpc(int i, int j) {
        boolean top = i > 1;
        boolean down = i < numberOfLines - 2;
        boolean lineUp = i > 0;
        boolean lineDown = i < numberOfLines - 1;
        boolean parity = i % 2 == 0;

        ArrayList<Point> arrayList = new ArrayList<>();

        if (parity) {
            boolean left = j > 1;
            boolean right = j < numberOfColumns - 2;

            if (lineUp) {
                if (left) {
                    arrayList.add(new Point(i - 1, j - 2));
                }
                if (right) {
                    arrayList.add(new Point(i - 1, j + 1));
                }
            }

            if (lineDown) {
                if (left) {
                    arrayList.add(new Point(i + 1, j - 2));
                }
                if (right) {
                    arrayList.add(new Point(i + 1, j + 1));
                }
            }
        }

        else {
            boolean left = j > 0;
            boolean right = j < numberOfColumns - 1;

            if (lineUp) {
                if (left) {
                    arrayList.add(new Point(i - 1, j - 1));
                }
                if (right) {
                    arrayList.add(new Point(i - 1, j + 2));
                }
            }

            if (lineDown) {
                if (left) {
                    arrayList.add(new Point(i + 1, j - 1));
                }
                if (right) {
                    arrayList.add(new Point(i + 1, j + 2));
                }
            }
        }


        if (down) {
            arrayList.add(new Point(i + 2, j ));
        }
        if (top) {
            arrayList.add(new Point(i - 2, j));
        }

        return arrayList;
    }


    public void resetParams(int numberOfColumns, int numberOfLines, int radius, int lineThickness) {
        this.numberOfColumns = numberOfColumns;
        this.numberOfLines = numberOfLines;
        this.lineThickness = lineThickness;
        this.innerRadius = radius;

        double temp =  lineThickness == 1 ?
                innerRadius
                : innerRadius + lineThickness;
        this.deltaY = lineThickness == 1 ?
                innerRadius * 3 / 2
                : (int)Math.ceil(innerRadius * 3 / 2 + lineThickness * 3 / 2);
        this.deltaXY = lineThickness == 1 ?
                (int)Math.ceil(temp * Math.sqrt(3) / 2)
                : (int)Math.ceil(temp * Math.sqrt(3) / 2) + (lineThickness + 1) % 2 - (innerRadius % 2);
        this.deltaX = lineThickness == 1 ?
                (int)Math.ceil(temp * Math.sqrt(3) / 2) * 2
                : (int)Math.ceil(temp * Math.sqrt(3) / 2) * 2 - (innerRadius % 2);
        this.radius = lineThickness == 1 ?
                innerRadius
                : (int)Math.ceil(temp);

        deltaX = deltaXY * 2 == deltaX ?
                deltaX:
                deltaX--;

        x = deltaXY + 15;
        startY = deltaY;
    }

    private boolean isHex(Point p){
        int length = numberOfColumns - 1;

        for (int i = 0; i < numberOfLines; i++) {
            if (i % 2 == 0) {
                length++;
            } else {
                length--;
            }
            for (int j = 0; j < length; j++) {
                if (hexagons[i][j].isHex(p, innerRadius)) {
                    mouseClicked = new Point(i, j);
                    return true;
                }
            }
        }
        return false;
    }

    private class scheduler extends TimerTask{
        @Override
        public void run() {
            if(life.isTriggered()) {
                life.nextStep();
                refillHexGrid();
            }
            else
                stop();
        }
    }

    public void drawCenteredString(String text, int x, int y) {
        Font font = new Font("Plain", Font.PLAIN, innerRadius / 2);
        g.setFont(font);
        g.setColor(Color.DARK_GRAY.darker());

        FontMetrics metrics = g.getFontMetrics(font);

        x -= metrics.stringWidth(text) / 2 + (innerRadius % 2 == 0 ? 0 : 1);
        y += (innerRadius / 4 - 1);

        g.drawString(text, x, y);
    }

    private class CustomListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (!isPlay) {
                Point point = e.getPoint();

                if (isHex(point)) {
                    if(XOR) {
                        hexagons[mouseClicked.x][mouseClicked.y].colored = true;
                        life.isLiving[mouseClicked.x][mouseClicked.y]
                                = !life.isLiving[mouseClicked.x][mouseClicked.y];

                        xor.x = mouseClicked.x;
                        xor.y = mouseClicked.y;
                    }
                    else
                        life.isLiving[mouseClicked.x][mouseClicked.y] = true;
                    life.calculateImpacts();
                    fillImpc(mouseClicked);
                    repaint();
                }
            }
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (!isPlay) {
                Point point = e.getPoint();
                    if (isHex(point)) {
                        if (XOR) {
                            if (!hexagons[mouseClicked.x][mouseClicked.y].colored) {
                                hexagons[mouseClicked.x][mouseClicked.y].colored = true;
                                life.isLiving[mouseClicked.x][mouseClicked.y]
                                        = !life.isLiving[mouseClicked.x][mouseClicked.y];
                            }

                            if (xor.x != mouseClicked.x || xor.y != mouseClicked.y) {
                                hexagons[xor.x][xor.y].colored = false;
                                xor.x = mouseClicked.x;
                                xor.y = mouseClicked.y;
                            }
                        }
                        else {
                            life.isLiving[mouseClicked.x][mouseClicked.y] = true;
                        }
                        life.calculateImpacts();
                        fillImpc(mouseClicked);
                    }
                repaint();
            }
        }
    }
}