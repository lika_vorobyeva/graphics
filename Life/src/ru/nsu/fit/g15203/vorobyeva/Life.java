package ru.nsu.fit.g15203.vorobyeva;

public class Life {
    private static double FST_IMPACT;
    private static double SND_IMPACT;
    private static double LIVE_BEGIN;
    private static double LIVE_END;
    private static double BIRTH_BEGIN;
    private static double BIRTH_END;

    double[][] prevImpact;
    double[][] impact;

    public boolean[][] isLiving;
    private boolean parity;

    private int fst_count = 0;
    private int snd_count = 0;

    public int numberOfLines;
    public int numberOfColumns;

    private  int length;


    public Life(int x, int y, double FST_IMPACT, double SND_IMPACT, double LIVE_BEGIN,
                double LIVE_END, double BIRTH_BEGIN, double BIRTH_END) {
        impact = new double[x][y];
        prevImpact = new double[x][y];
        isLiving = new boolean[x][y];

        this.FST_IMPACT = FST_IMPACT;
        this.SND_IMPACT = SND_IMPACT;
        this.LIVE_BEGIN = LIVE_BEGIN;
        this.LIVE_END = LIVE_END;
        this.BIRTH_BEGIN = BIRTH_BEGIN;
        this.BIRTH_END = BIRTH_END;

        this.numberOfLines = x;
        this.numberOfColumns = y;

        initLife();
    }

    public void nextStep() {
        int length = numberOfColumns - 1;

        for (int i = 0; i < numberOfLines; i++) {
            if (i % 2 == 0) {
                length++;
                parity = true;
            } else {
                length--;
                parity = false;
            }

            for (int j = 0; j < length; j++) {
                if (!isLiving[i][j] && goingToBorn(i, j))
                    isLiving[i][j] = true;

                if (isLiving[i][j] && isAlive(i, j))
                    isLiving[i][j] = true;
                else
                    isLiving[i][j] = false;
            }
        }

        calculateImpacts();
    }

    public void calculateImpacts() {
        prevImpact = impact;
        double[][] temp = prevImpact;
        length = numberOfColumns - 1;

        for (int i = 0; i < numberOfLines; i++) {
            if (i % 2 == 0) {
                length++;
                parity = true;
            } else {
                length--;
                parity = false;
            }

            for (int j = 0; j < length; j++) {
                fst_count = 0;
                snd_count = 0;

                calculateFirstImpact(i, j);
                calculateSecondImpact(i, j);

                temp[i][j] = FST_IMPACT * fst_count + SND_IMPACT * snd_count;
            }
        }

        impact = temp;
    }

    private void calculateFirstImpact(int i, int j) {
        boolean top = i > 0;
        boolean down = i < impact.length - 1;
        boolean left = j > 0;
        boolean right = j < length - 1;

        if (right && isLiving[i][j + 1]) {
            fst_count++;
        }
        if (left && isLiving[i][j - 1]) {
            fst_count++;
        }

        if (top){
            if (parity) {
                if (left && isLiving[i - 1][j - 1]) {
                    fst_count++;
                }
                if (right && isLiving[i - 1][j]) {
                    fst_count++;
                }
            }
            else {
                if (isLiving[i - 1][j + 1]) {
                    fst_count++;
                }
                if (isLiving[i - 1][j]) {
                    fst_count++;
                }
            }
        }

        if (down) {
            if (parity) {
                if (left && isLiving[i + 1][j - 1]) {
                    fst_count++;
                }
                if (right && isLiving[i + 1][j]) {
                    fst_count++;
                }
            }

            else  {
                if (isLiving[i + 1][j]) {
                    fst_count++;
                }
                if(isLiving[i + 1][j + 1]) {
                    fst_count++;
                }
            }
        }
    }

    private void calculateSecondImpact(int i, int j) {
        boolean top = i > 1;
        boolean down = i < impact.length - 2;
        boolean lineUp = i > 0;
        boolean lineDown = i < impact.length - 1;

        if (parity) {
            boolean left = j > 1;
            boolean right = j < length - 2;

            if (lineUp) {
                if (left && isLiving[i - 1][j - 2]) {
                    snd_count++;
                }
                if (right && isLiving[i - 1][j + 1]) {
                    snd_count++;
                }
            }

            if (lineDown) {
                if (left && isLiving[i + 1][j - 2]) {
                    snd_count++;
                }
                if (right && isLiving[i + 1][j + 1]) {
                    snd_count++;
                }
            }
        }

        else {
            boolean left = j > 0;
            boolean right = j < length - 1;

            if (lineUp) {
                if (left && isLiving[i - 1][j - 1]) {
                    snd_count++;
                }
                if (right && isLiving[i - 1][j + 2]) {
                    snd_count++;
                }
            }

            if (lineDown) {
                if (left && isLiving[i + 1][j - 1]) {
                    snd_count++;
                }
                if (right && isLiving[i + 1][j + 2]) {
                    snd_count++;
                }
            }
        }


        if (down && isLiving[i + 2][j]) {
            snd_count++;
        }
        if (top && isLiving[i - 2][j]) {
            snd_count++;
        }
    }

    public boolean isAlive(int x, int y) {
        return prevImpact[x][y] >= Life.LIVE_BEGIN && prevImpact[x][y] <= Life.LIVE_END;
    }


    public boolean goingToBorn(int x, int y) {
        return prevImpact[x][y] >= Life.BIRTH_BEGIN && prevImpact[x][y] <= Life.BIRTH_END;
    }

    public void initLife() {
        for(int i = 0; i < impact.length; ++i) {
            for(int j = 0; j < impact[i].length; ++j){
                impact[i][j] = 0;
                prevImpact[i][j] = 0;
                isLiving[i][j] = false;
                }
        }

        calculateImpacts();
    }

    public boolean isTriggered() {
        int length = numberOfColumns - 1;

        for (int i = 0; i < numberOfLines; i++) {
            if (i % 2 == 0) {
                length++;
            } else {
                length--;
            }
            for (int j = 0; j < length; j++) {
                if(isLiving[i][j])
                    return true;
            }
        }
        return false;
    }
}
