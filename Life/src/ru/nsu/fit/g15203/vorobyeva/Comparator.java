package ru.nsu.fit.g15203.vorobyeva;

import java.util.Arrays;

public class Comparator {
    private int thicknessToCompare;
    private int radiusToCompare;
    private int columnsToCompare;
    private int linesToCompare;
    private boolean[][] isLivingToCompare;

    public Comparator(int columnsToCompare, int linesToCompare, int radiusToCompare, int thicknessToCompare,
                      boolean[][] isLivingToCompare){
        this.columnsToCompare = columnsToCompare;
        this.linesToCompare = linesToCompare;
        this.radiusToCompare = radiusToCompare;
        this.thicknessToCompare = thicknessToCompare;

        this.isLivingToCompare = new boolean[isLivingToCompare.length][isLivingToCompare.length];

        for (int i = 0; i < isLivingToCompare.length; i++) {
            this.isLivingToCompare[i] = isLivingToCompare[i].clone();
        }
    }

    public boolean equal(int columnsToCompare, int linesToCompare, int radiusToCompare, int thicknessToCompare,
        boolean[][] isLivingToCompare){

        if(columnsToCompare != this.columnsToCompare || linesToCompare != this.linesToCompare
                || radiusToCompare != this.radiusToCompare || thicknessToCompare != this.thicknessToCompare)
            return false;

        for (int i = 0; i < isLivingToCompare.length; i++) {
            if (!Arrays.equals(this.isLivingToCompare[i], isLivingToCompare[i]))
                return false;
        }

        return true;
    }
}

