package ru.nsu.fit.g15203.vorobyeva;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;


public class MainFrame {
    private JMenuBar menuBar = new JMenuBar();
    private JFrame jFrame = new JFrame();
    private HexGrid hexGrid;
    private JScrollPane scrollPane;
    private JLabel statusBar;
    private JToolBar toolBar;
    private Life life;
    private FileSystem fileSystem;
    private Comparator comparator;

    private final int initLineThickness = 6;
    private final int initInnerRadius = 60;
    private final int initNumberOfColumns = 40;
    private final int initNumberOfLines = 40;

    private Integer numberOfColumns = initNumberOfColumns;
    private Integer numberOfLines = initNumberOfLines;
    private Integer innerRadius = initInnerRadius;
    private Integer lineThickness = initLineThickness;

    private static final double initFST_IMPACT = 1.0;
    private static final double initSND_IMPACT = 0.3;
    private static final double initLIVE_BEGIN = 2.0;
    private static final double initLIVE_END = 3.3;
    private static final double initBIRTH_BEGIN = 2.3;
    private static final double initBIRTH_END = 2.9;

    private double FST_IMPACT = initFST_IMPACT;
    private double SND_IMPACT = initSND_IMPACT;
    private double LIVE_BEGIN = initLIVE_BEGIN;
    private double LIVE_END = initLIVE_END;
    private double BIRTH_BEGIN = initBIRTH_BEGIN;
    private double BIRTH_END = initBIRTH_END;
    private double temp;
    private String current;

    private boolean xorMode = false;
    private boolean impacts = false;
    private boolean saved = false;
    private boolean needUpdating = false;


    public MainFrame() {
        MatteBorder matteBorder = new MatteBorder(1, 0, 0, 0, Color.BLACK);
        statusBar = new JLabel("Ready");
        ButtonGroup group = new ButtonGroup();
        ButtonGroup tGroup = new ButtonGroup();

        life = new Life(numberOfLines, numberOfColumns, initFST_IMPACT, initSND_IMPACT,
                initLIVE_BEGIN, initLIVE_END, initBIRTH_BEGIN, initBIRTH_END);
        fileSystem = new FileSystem(initNumberOfColumns, initNumberOfLines,
                initInnerRadius, initLineThickness, life);
        comparator = new Comparator(initNumberOfColumns, initNumberOfLines,
                initInnerRadius, initLineThickness, life.isLiving);
        hexGrid = new HexGrid(initNumberOfColumns, initNumberOfLines,
                initInnerRadius, initLineThickness, life);
        hexGrid.setBackground(Color.WHITE);

        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        panel.add(hexGrid);

        scrollPane = new JScrollPane(panel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        JMenu fileMenu = new JMenu("File");
        JMenuItem jmiNG = new JMenuItem("New Game");
        fileMenu.add(jmiNG);
        JMenuItem jmOpen = new JMenuItem("Open");
        fileMenu.add(jmOpen);
        JMenuItem jmSave = new JMenuItem("Save");
        fileMenu.add(jmSave);
        JMenuItem jmSaveAs = new JMenuItem("Save As...");
        fileMenu.add(jmSaveAs);
        fileMenu.addSeparator();
        JMenuItem jmExit= new JMenuItem("Exit");
        jmExit.addActionListener(e -> {
            if(!comparator.equal(numberOfColumns,numberOfLines,innerRadius,lineThickness,life.isLiving)) {
                int result = JOptionPane.showConfirmDialog(jFrame,
                        "Do you want to save the progress?",
                        "Exit",
                        JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.YES_OPTION) {
                    fileSystem.saveState(jFrame);
                }
            }
            System.exit(0);}
        );
        fileMenu.add(jmExit);

        JMenu modifyMenu = new JMenu("Modify");
        JMenuItem jmOptions = new JMenuItem("Options");
        modifyMenu.add(jmOptions);
        JMenuItem jmReplace = new JCheckBoxMenuItem("Replace");
        jmReplace.setSelected(!xorMode);
        modifyMenu.add(jmReplace);
        group.add(jmReplace);
        JMenuItem jmXOR = new JCheckBoxMenuItem("XOR");
        jmXOR.setSelected(xorMode);
        modifyMenu.add(jmXOR);
        group.add(jmXOR);
        JMenuItem jmImpacts = new JCheckBoxMenuItem("Impacts");
        jmImpacts.setSelected(impacts);
        modifyMenu.add(jmImpacts);

        JMenu actionMenu = new JMenu("Action");
        JMenuItem jmNext = new JMenuItem("Next step");
        actionMenu.add(jmNext);
        JMenuItem jmRun = new JMenuItem("Play!");
        actionMenu.add(jmRun);
        JMenuItem jmStop = new JMenuItem("Stop");
        actionMenu.add(jmStop);
        actionMenu.addSeparator();
        JMenuItem jmClear = new JMenuItem("Clear");
        actionMenu.add(jmClear);
        JMenu helpMenu = new JMenu("Help");
        JMenuItem jmiAbout = new JMenuItem("About");
        helpMenu.add(jmiAbout);

        menuBar.setBorder(matteBorder);
        menuBar.add(fileMenu);
        menuBar.add(actionMenu);
        menuBar.add(modifyMenu);
        menuBar.add(helpMenu);

        toolBar = new JToolBar();
        toolBar.setBorder(matteBorder);
        toolBar.setRollover(true);

        String[] iconFiles = {"src/resourses/new.png",
                "src/resourses/open.png",
                "src/resourses/save.png",
                "src/resourses/play.png",
                "src/resourses/next.png",
                "src/resourses/stop.png",
                "src/resourses/replace.png",
                "src/resourses/xor.png",
                "src/resourses/number.png",
                "src/resourses/close.png",
                "src/resourses/settings.png",
                "src/resourses/about.png"};
        String[] buttonLabels = {"New", "Open", "Save",
                "Play", "Next", "Stop", "Replace", "Xor", "Impacts",  "Clear",
                "Options", "About"};
        JButton[] buttons = new JButton[6];
        ImageIcon[] icons = new ImageIcon[iconFiles.length];

        for (int i = 0; i < 3; i++) {
            icons[i] = new ImageIcon(iconFiles[i]);
            buttons[i] = new JButton(icons[i]);
            buttons[i].setToolTipText(buttonLabels[i]);

            if (i == 3)
                toolBar.addSeparator();

            toolBar.add(buttons[i]);
        }

        JToggleButton tPlay = new JToggleButton(new ImageIcon(iconFiles[3]));
        tPlay.setToolTipText(buttonLabels[3]);
        tPlay.setSelected(false);
        toolBar.add(tPlay);

        JButton tNext = new JButton(new ImageIcon(iconFiles[4]));
        tNext.setToolTipText(buttonLabels[4]);
        tNext.setSelected(false);
        toolBar.add(tNext);

        JToggleButton tStop = new JToggleButton(new ImageIcon(iconFiles[5]));
        tStop.setToolTipText(buttonLabels[5]);
        tStop.setSelected(true);
        toolBar.add(tStop);

        JToggleButton tReplace = new JToggleButton(new ImageIcon(iconFiles[6]));
        tReplace.setToolTipText(buttonLabels[6]);
        tReplace.setSelected(!xorMode);
        tGroup.add(tReplace);
        toolBar.add(tReplace);

        JToggleButton tXor = new JToggleButton(new ImageIcon(iconFiles[7]));
        tXor.setToolTipText(buttonLabels[7]);
        tReplace.setSelected(xorMode);
        tGroup.add(tXor);
        toolBar.add(tXor);

        JToggleButton tImpacts = new JToggleButton(new ImageIcon(iconFiles[8]));
        tImpacts.setToolTipText(buttonLabels[8]);
        tImpacts.setSelected(impacts);
        toolBar.add(tImpacts);


        for (int i = 3; i < 6; i++) {
            icons[i] = new ImageIcon(iconFiles[i + 6]);

            if(i == 7)
                toolBar.addSeparator();

            buttons[i] = new JButton(icons[i]);
            buttons[i].setToolTipText(buttonLabels[i + 6]);
            toolBar.add(buttons[i]);
        }


        ActionListener xor = e -> {
            xorMode = true;
            hexGrid.XOR = xorMode;
            jmXOR.setSelected(xorMode);
            tXor.setSelected(xorMode);
        };

        ActionListener replace = e -> {
            xorMode = false;
            hexGrid.XOR = xorMode;
            jmReplace.setSelected(!xorMode);
            tReplace.setSelected(!xorMode);
        };

        ActionListener showImpacts = e -> {
            impacts = !impacts;
            hexGrid.showImpacts = impacts;
            hexGrid.refillHexGrid();
            tImpacts.setSelected(impacts);
            jmImpacts.setSelected(impacts);
        };

        ActionListener clear = e -> {
            hexGrid.stop();
            tStop.setSelected(true);
            tPlay.setSelected(false);
            jmRun.setEnabled(true);
            jmNext.setEnabled(true);
            tPlay.setEnabled(true);
            tNext.setEnabled(true);
            life.initLife();
            hexGrid.resetGrid();
        };

        ActionListener about = e ->
                JOptionPane.showMessageDialog(jFrame, "FIT_15203_Vorobyeva_Lika_Life\n"+
                        "Author:   Vorobyeva Lika\n"+
                                "Year:  2018\n",
                        "About", JOptionPane.INFORMATION_MESSAGE);

        ActionListener next = e -> {
            hexGrid.nextStep();
            tStop.setSelected(false);
        };

        ActionListener play = e -> {
            if (life.isTriggered()) {
                hexGrid.play();
                tPlay.setSelected(true);
                tStop.setSelected(false);
                jmRun.setEnabled(false);
                jmNext.setEnabled(false);
                tPlay.setEnabled(false);
                tNext.setEnabled(false);
            }
            else {
                tPlay.setSelected(false);
                tPlay.setEnabled(true);
                tNext.setEnabled(true);
            }
        };

        ActionListener stop = e -> {
            hexGrid.stop();
            tStop.setSelected(true);
            tPlay.setSelected(false);
            jmRun.setEnabled(true);
            jmNext.setEnabled(true);
            tPlay.setEnabled(true);
            tNext.setEnabled(true);
        };

        ActionListener open = e->{
            hexGrid.stop();
            tStop.setSelected(true);
            tPlay.setSelected(false);
            jmRun.setEnabled(true);
            jmNext.setEnabled(true);
            tPlay.setEnabled(true);
            tNext.setEnabled(true);
            if(!comparator.equal(numberOfColumns, numberOfLines, innerRadius, lineThickness, life.isLiving)) {
                int result = JOptionPane.showConfirmDialog(jFrame,
                        "Do you want to save the progress?",
                        "Open file",
                        JOptionPane.YES_NO_CANCEL_OPTION);

                if (result == JOptionPane.YES_OPTION) {
                    fileSystem.saveState(jFrame);
                }
            }

            fileSystem.loadFromFile(jFrame);
            if(fileSystem.reload) {
                saved = true;

                numberOfColumns = fileSystem.numberOfColumns;
                numberOfLines = fileSystem.numberOfLines;
                innerRadius = fileSystem.radius;
                lineThickness = fileSystem.lineThickness;
                xorMode = hexGrid.XOR;

                life = new Life(numberOfLines,numberOfColumns,
                        initFST_IMPACT,initSND_IMPACT,initLIVE_BEGIN,
                        initLIVE_END, initBIRTH_BEGIN, initBIRTH_END);
                life.isLiving = fileSystem.life.isLiving;

                life.calculateImpacts();

                current = fileSystem.currentFilename;
                saved = fileSystem.saved;
                fileSystem = new FileSystem(numberOfColumns, numberOfLines,
                        innerRadius, lineThickness, life);
                fileSystem.currentFilename = current;
                fileSystem.saved = saved;
                comparator = new Comparator(numberOfColumns, numberOfLines,
                        innerRadius, lineThickness, life.isLiving);
                hexGrid = new HexGrid(numberOfColumns, numberOfLines,
                        innerRadius, lineThickness, life);
                hexGrid.setBackground(Color.WHITE);
                panel.updateUI();
                panel.removeAll();
                hexGrid.XOR = xorMode;

                if (impacts) {
                    hexGrid.showImpacts = impacts;
                    hexGrid.refillHexGrid();
                }

                panel.add(hexGrid);
                panel.updateUI();
                panel.setBackground(Color.WHITE);
            }
        };

        ActionListener save = e -> {
            hexGrid.stop();
            tStop.setSelected(true);
            tPlay.setSelected(false);
            jmRun.setEnabled(true);
            jmNext.setEnabled(true);
            tPlay.setEnabled(true);
            tNext.setEnabled(true);
            fileSystem.saved = saved;
            if(fileSystem.saveState(jFrame)) {
                comparator = new Comparator(numberOfColumns, numberOfLines,
                        innerRadius, lineThickness, life.isLiving);
                saved = true;
            }
        };

        ActionListener saveAs = e -> {
            hexGrid.stop();
            tStop.setSelected(true);
            tPlay.setSelected(false);
            jmRun.setEnabled(true);
            jmNext.setEnabled(true);
            tPlay.setEnabled(true);
            tNext.setEnabled(true);
            fileSystem.saved = false;
            if(fileSystem.saveState(jFrame)) {
                comparator = new Comparator(numberOfColumns, numberOfLines,
                        innerRadius, lineThickness, life.isLiving);
                saved = true;
            }
        };

        ActionListener options = e -> {
            hexGrid.stop();
            tStop.setSelected(true);
            tPlay.setSelected(false);
            jmRun.setEnabled(true);
            jmNext.setEnabled(true);
            tPlay.setEnabled(true);
            tNext.setEnabled(true);
            refresh(panel);
            jmReplace.setSelected(!xorMode);
            tReplace.setSelected(!xorMode);
            jmXOR.setSelected(xorMode);
            tXor.setSelected(xorMode);
            tImpacts.setSelected(impacts);
            jmImpacts.setSelected(impacts);
        };


        ActionListener newGame = e -> {
            hexGrid.stop();
            tStop.setSelected(true);
            tPlay.setSelected(false);
            jmRun.setEnabled(true);
            jmNext.setEnabled(true);
            tPlay.setEnabled(true);
            tNext.setEnabled(true);
            int result = -1;
            if(!comparator.equal(numberOfColumns, numberOfLines, innerRadius, lineThickness, life.isLiving)) {
                result = JOptionPane.showConfirmDialog(jFrame,
                        "Do you want to save the progress?",
                        "New Game",
                        JOptionPane.YES_NO_CANCEL_OPTION);

                if (result == JOptionPane.YES_OPTION) {
                    fileSystem.saved = saved;
                    fileSystem.saveState(jFrame);
                    current = fileSystem.currentFilename;
                }
            }
            if (result == JOptionPane.NO_OPTION || result == JOptionPane.YES_OPTION || result == -1) {
                saved = false;
                impacts = false;
                xorMode = false;
                needUpdating = false;
                hexGrid.showImpacts = false;
                life.initLife();

                lineThickness = initLineThickness;
                innerRadius = initInnerRadius;
                numberOfColumns = initNumberOfColumns;
                numberOfLines = initNumberOfLines;

                FST_IMPACT = initFST_IMPACT;
                SND_IMPACT = initSND_IMPACT;
                LIVE_BEGIN = initLIVE_BEGIN;
                LIVE_END = initLIVE_END;
                BIRTH_BEGIN = initBIRTH_BEGIN;
                BIRTH_END = initBIRTH_END;

                refresh(panel);

                jmReplace.setSelected(!xorMode);
                tReplace.setSelected(!xorMode);
                tImpacts.setSelected(impacts);
                jmImpacts.setSelected(impacts);
            }
        };

        jmiNG.addActionListener(newGame);
        jmOpen.addActionListener(open);
        jmSave.addActionListener(save);
        jmSaveAs.addActionListener(saveAs);
        jmOptions.addActionListener(options);
        jmNext.addActionListener(next);
        jmRun.addActionListener(play);
        jmStop.addActionListener(stop);
        jmClear.addActionListener(clear);
        jmiAbout.addActionListener(about);

        buttons[0].addActionListener(newGame);
        buttons[1].addActionListener(open);
        buttons[2].addActionListener(save);

        tPlay.addActionListener(play);
        tNext.addActionListener(next);
        tStop.addActionListener(stop);

        tReplace.addActionListener(replace);
        jmReplace.addActionListener(replace);
        tXor.addActionListener(xor);
        jmXOR.addActionListener(xor);
        tImpacts.addActionListener(showImpacts);
        jmImpacts.addActionListener(showImpacts);

        buttons[3].addActionListener(clear);
        buttons[4].addActionListener(options);
        buttons[5].addActionListener(about);
    }

    public void init() {
        jFrame.setPreferredSize(new Dimension(800, 600));
        jFrame.setMinimumSize(new Dimension(800, 600));
        jFrame.setLayout(new BorderLayout());

        jFrame.add(statusBar, BorderLayout.SOUTH);
        jFrame.add(scrollPane, BorderLayout.CENTER);
        jFrame.setJMenuBar(menuBar);
        jFrame.add(toolBar, BorderLayout.NORTH);

        jFrame.setTitle("Life");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        WindowListener winListener = new MyWindowsListener();
        jFrame.addWindowListener(winListener);
        jFrame.pack();
        jFrame.setVisible(true);
        jFrame.setResizable(true);
        jFrame.setLocationRelativeTo(null);
    }

    private void settingsDialog() {
        final JDialog dialog = new JDialog(jFrame, "Settings", true);
        DigitFilter digitFilter = new DigitFilter();
        DoubleFilter doubleFilter = new DoubleFilter();

        JPanel mainPanel = new JPanel(new GridLayout(1, 7));
        JPanel panelName = new JPanel(new GridLayout(6, 1));
        JPanel panelSliders = new JPanel(new GridLayout(6, 1));
        JPanel panelValues = new JPanel(new GridLayout(6, 1));

        JPanel panelBegin = new JPanel(new GridLayout(6,1));
        JPanel panelValuesForBegin = new JPanel(new GridLayout(6,1));
        JPanel panelEnd = new JPanel(new GridLayout(6,1));
        JPanel panelValuesForEnd = new JPanel(new GridLayout(6,1));

        JLabel labelOne = new JLabel("Columns", SwingConstants.CENTER);
        JTextField columnsField = new JTextField();
        PlainDocument doc = (PlainDocument) columnsField.getDocument();
        doc.setDocumentFilter(digitFilter);

        JSlider jSliderColumns = new JSlider(JSlider.HORIZONTAL, 1, 40, 1);
        columnsField.setText(numberOfColumns.toString());
        jSliderColumns.setValue(numberOfColumns);

        panelName.add(labelOne);
        panelSliders.add(jSliderColumns);
        panelValues.add(columnsField);

        JLabel labelTwo = new JLabel("Rows", SwingConstants.CENTER);
        JTextField linesField = new JTextField();
        PlainDocument docH = (PlainDocument) linesField.getDocument();
        docH.setDocumentFilter(digitFilter);

        JSlider jSliderLines = new JSlider(JSlider.HORIZONTAL,1,40,1);
        linesField.setText(numberOfLines.toString());
        jSliderLines.setValue(numberOfLines);

        panelName.add(labelTwo);
        panelSliders.add(jSliderLines);
        panelValues.add(linesField);

        JLabel labelThree = new JLabel("Radius", SwingConstants.CENTER);
        JTextField radiusField = new JTextField();
        PlainDocument docR = (PlainDocument) radiusField.getDocument();
        docR.setDocumentFilter(digitFilter);
        JSlider jSliderRadius = new JSlider(JSlider.HORIZONTAL,8,60,10);

        radiusField.setText(innerRadius.toString());
        jSliderRadius.setValue(innerRadius);

        panelName.add(labelThree);
        panelSliders.add(jSliderRadius);
        panelValues.add(radiusField);

        JLabel labelFour = new JLabel("Line Thickness", SwingConstants.CENTER);
        JTextField lineThicknessField = new JTextField();
        PlainDocument docL = (PlainDocument) lineThicknessField.getDocument();
        docL.setDocumentFilter(digitFilter);
        JSlider jSliderThickness = new JSlider(JSlider.HORIZONTAL,1,10,1);

        lineThicknessField.setText(lineThickness.toString());
        jSliderThickness.setValue(lineThickness);

        panelName.add(labelFour);
        panelSliders.add(jSliderThickness);
        panelValues.add(lineThicknessField);

        ButtonGroup group = new ButtonGroup();
        JRadioButton xorButton = new JRadioButton("XOR", false);
        xorButton.setSelected(xorMode);
        group.add(xorButton);

        JRadioButton replaceButton = new JRadioButton("Replace", true);
        replaceButton.setSelected(!xorMode);
        group.add(replaceButton);

        JButton ok = new JButton("OK");

        panelSliders.add(xorButton);
        panelValues.add(replaceButton);
        panelValues.add(ok);

        JLabel labelFST = new JLabel("FST_IMPACT", SwingConstants.CENTER);
        JTextField jFST = new JTextField();
        PlainDocument docFST = (PlainDocument)jFST.getDocument();
        docFST.setDocumentFilter(doubleFilter);
        if (FST_IMPACT % 1 == 0.00){
            jFST.setText(String.format("%.0f", FST_IMPACT).replace(",","."));
        } else
            jFST.setText(String.format("%.1f", FST_IMPACT).replace(",","."));

        panelBegin.add(labelFST);
        panelValuesForBegin.add(jFST);

        JLabel labelLB = new JLabel("LIVE_BEGIN", SwingConstants.CENTER);
        JTextField jLB = new JTextField();
        PlainDocument docLB = (PlainDocument) jLB.getDocument();
        docLB.setDocumentFilter(doubleFilter);
        if (LIVE_BEGIN % 1 == 0.00){
            jLB.setText(String.format("%.0f", LIVE_BEGIN).replace(",","."));
        } else
            jLB.setText(String.format("%.1f", LIVE_BEGIN).replace(",","."));

        panelBegin.add(labelLB);
        panelValuesForBegin.add(jLB);

        JLabel labelBB = new JLabel("BIRTH_BEGIN", SwingConstants.CENTER);
        JTextField jBB = new JTextField();
        PlainDocument docBB = (PlainDocument) jBB.getDocument();
        docBB.setDocumentFilter(doubleFilter);
        if (BIRTH_BEGIN % 1 == 0.00){
            jBB.setText(String.format("%.0f", BIRTH_BEGIN).replace(",","."));
        } else
            jBB.setText(String.format("%.1f", BIRTH_BEGIN).replace(",","."));

        panelBegin.add(labelBB);
        panelValuesForBegin.add(jBB);

        JLabel labelSND = new JLabel("SND_IMPACT", SwingConstants.CENTER);
        JTextField jSND = new JTextField();
        PlainDocument docSND = (PlainDocument) jSND.getDocument();
        docSND.setDocumentFilter(doubleFilter);
        if (SND_IMPACT % 1 == 0.00){
            jSND.setText(String.format("%.0f", SND_IMPACT).replace(",","."));
        } else
            jSND.setText(String.format("%.1f", SND_IMPACT).replace(",","."));

        panelEnd.add(labelSND);
        panelValuesForEnd.add(jSND);

        JLabel labelLE = new JLabel("LIVE_END", SwingConstants.CENTER);
        JTextField jLE = new JTextField();
        PlainDocument docLE = (PlainDocument) jLE.getDocument();
        docLE.setDocumentFilter(doubleFilter);
        if (LIVE_END % 1 == 0.00){
            jLE.setText(String.format("%.0f", LIVE_END).replace(",","."));
        } else
            jLE.setText(String.format("%.1f", LIVE_END).replace(",","."));

        panelEnd.add(labelLE);
        panelValuesForEnd.add(jLE);

        JLabel labelBE = new JLabel("BIRTH_END", SwingConstants.CENTER);
        JTextField jBE = new JTextField();
        PlainDocument docBE = (PlainDocument) jBE.getDocument();
        docBE.setDocumentFilter(doubleFilter);
        if (BIRTH_END % 1 == 0.00){
            jBE.setText(String.format("%.0f", BIRTH_END).replace(",","."));
        } else
            jBE.setText(String.format("%.1f", BIRTH_END).replace(",","."));

        panelEnd.add(labelBE);
        panelValuesForEnd.add(jBE);

        mainPanel.add(panelName);
        mainPanel.add(panelSliders);
        mainPanel.add(panelValues);
        mainPanel.add(panelBegin);
        mainPanel.add(panelValuesForBegin);
        mainPanel.add(panelEnd);
        mainPanel.add(panelValuesForEnd);
        dialog.add(mainPanel);

        ok.addActionListener(e-> {
            int tag = 0;

            if (!columnsField.getText().isEmpty())
                numberOfColumns = Integer.parseInt(columnsField.getText());

            if (!linesField.getText().isEmpty())
                numberOfLines = Integer.parseInt(linesField.getText());

            if (!radiusField.getText().isEmpty()) {
                innerRadius = Integer.parseInt(radiusField.getText());
                if (innerRadius < 8)
                    innerRadius = 8;
            }

            if (!lineThicknessField.getText().isEmpty())
                lineThickness = Integer.parseInt(lineThicknessField.getText());

            if (!jFST.getText().isEmpty())
                try {
                    FST_IMPACT = Double.parseDouble(jFST.getText().replace(",", "."));
                } catch (NumberFormatException n) {
                }

            if (!jSND.getText().isEmpty())
                try {
                    SND_IMPACT = Double.parseDouble(jSND.getText().replace(",", "."));
                } catch (NumberFormatException n) {
                }

            if (!jLB.getText().isEmpty()) {
                try {
                    temp = Double.parseDouble(jLB.getText().replace(",", "."));
                    if (temp <= BIRTH_BEGIN) {
                        LIVE_BEGIN = temp;
                        tag++;
                    } else {
                        JOptionPane.showMessageDialog(jFrame,
                                        "LIVE_BEGIN should be LIVE_BEGIN <= BIRTH_BEGIN",
                                "Incorrect input", JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (NumberFormatException n) {}
            }


            if (!jBB.getText().isEmpty()) {
                try {
                    temp = Double.parseDouble(jBB.getText().replace(",", "."));
                    if (temp <= BIRTH_END && LIVE_BEGIN <= temp) {
                        BIRTH_BEGIN = temp;
                        tag++;
                    } else {
                        JOptionPane.showMessageDialog(jFrame,
                                "BIRTH_BEGIN should be BIRTH_BEGIN <= BIRTH_END",
                                "Incorrect input", JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (NumberFormatException n) {}
            }


            if (!jLE.getText().isEmpty()) {
                try {
                    temp = Double.parseDouble(jLE.getText().replace(",", "."));
                    if (BIRTH_END <= temp) {
                        LIVE_END = temp;
                        tag++;
                    } else {
                        JOptionPane.showMessageDialog(jFrame,
                                "LIVE_END should be BIRTH_END <= LIVE_END",
                                "Incorrect input", JOptionPane.INFORMATION_MESSAGE);
                    }

                } catch (NumberFormatException n) {}
            }

            if (!jBE.getText().isEmpty()) {
                try {
                    temp = Double.parseDouble(jBE.getText().replace(",", "."));
                    if (BIRTH_BEGIN <= temp && temp <= LIVE_END) {
                        BIRTH_END = temp;
                        tag++;
                    }
                    else {
                        JOptionPane.showMessageDialog(jFrame,
                                "BIRTH_END should be BIRTH_BEGIN <= BIRTH_END <= LIVE_END",
                                "Incorrect input", JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (NumberFormatException n) {}
             }
            if (tag == 4) {
                needUpdating = true;
                dialog.dispose();
            }
        });

        jSliderColumns.addChangeListener(e ->
            columnsField.setText(((Integer) ((JSlider) e.getSource()).getValue()).toString())
        );

        jSliderLines.addChangeListener(e ->
            linesField.setText(((Integer) ((JSlider) e.getSource()).getValue()).toString())
        );

        jSliderRadius.addChangeListener(e ->
            radiusField.setText(((Integer)((JSlider)e.getSource()).getValue()).toString())
        );

        jSliderThickness.addChangeListener(e ->
            lineThicknessField.setText(((Integer)((JSlider)e.getSource()).getValue()).toString())
        );

        columnsField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(!columnsField.getText().isEmpty())
                    jSliderColumns.setValue(Integer.parseInt(columnsField.getText()));
            }
        });

        linesField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(!linesField.getText().isEmpty())
                    jSliderLines.setValue(Integer.parseInt(linesField.getText()));
            }
        });

        radiusField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(!radiusField.getText().isEmpty())
                        jSliderRadius.setValue(Integer.parseInt(radiusField.getText()));
            }
        });

        lineThicknessField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(!lineThicknessField.getText().isEmpty())
                    jSliderThickness.setValue(Integer.parseInt(lineThicknessField.getText()));
            }
        });

        xorButton.addActionListener(e ->
            xorMode = true);
        replaceButton.addActionListener(e ->
            xorMode = false);

        dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        dialog.setPreferredSize(new Dimension(700,300));
        dialog.setResizable(false);
        dialog.pack();
        dialog.setLocationRelativeTo(this.jFrame);
        dialog.setVisible(true);
    }

    private void refresh(JPanel panel){
        settingsDialog();

        if (needUpdating) {
            impacts = hexGrid.showImpacts;
            ArrayList<Point> array = new ArrayList<>(life.numberOfColumns * life.numberOfLines);
            for (int i = 0; i < life.numberOfLines; i++)
                for (int j = 0; j < life.numberOfColumns - i % 2; j++)
                    if (life.isLiving[i][j])
                        array.add(new Point(i, j));

            life = new Life(numberOfLines, numberOfColumns, FST_IMPACT, SND_IMPACT, LIVE_BEGIN, LIVE_END, BIRTH_BEGIN, BIRTH_END);
            Point point;

            for (int i = 0; i < array.size(); i++) {
                point = array.get(i);
                if (array.get(i) == null) {
                    break;
                } else
                if (numberOfLines > point.x && numberOfColumns > point.y)
                    life.isLiving[point.x][point.y] = true;
            }

            life.calculateImpacts();
            current = fileSystem.currentFilename;

            fileSystem = new FileSystem(numberOfColumns, numberOfLines,
                    innerRadius, lineThickness, life);

            fileSystem.currentFilename = current;

            hexGrid = new HexGrid(numberOfColumns, numberOfLines,
                    innerRadius, lineThickness, life);
            hexGrid.setBackground(Color.WHITE);
            panel.updateUI();
            panel.removeAll();
            hexGrid.XOR = xorMode;

            if (impacts) {
                hexGrid.showImpacts = impacts;
                hexGrid.refillHexGrid();
            }
            panel.add(hexGrid);
            panel.updateUI();
            panel.setBackground(Color.WHITE);

            needUpdating = false;
        }

    }

    class DigitFilter extends DocumentFilter {
        private static final String DIGITS = "\\d+";

        @Override
        public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
            if (string.matches(DIGITS)) {
                super.insertString(fb, offset, string, attr);
            }
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String string, AttributeSet attrs) throws BadLocationException {
            if (string.matches(DIGITS)) {
                super.replace(fb, offset, length, string, attrs);
            }
        }
    }

    class DoubleFilter extends DocumentFilter {
        private static final String DIGITS = "\\d*\\.?\\d?$";

        @Override
        public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
            if (string.matches(DIGITS)) {
                super.insertString(fb, offset, string, attr);
            }
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String string, AttributeSet attrs) throws BadLocationException {
            if (string.matches(DIGITS)) {
                super.replace(fb, offset, length, string, attrs);
            }
        }
    }

    private class MyWindowsListener implements WindowListener {
        @Override
        public void windowOpened(WindowEvent e) {
        }

        @Override
        public void windowClosing(WindowEvent e) {
            if(!comparator.equal(numberOfColumns,numberOfLines,innerRadius,lineThickness,life.isLiving)) {
                int result = JOptionPane.showConfirmDialog(null,
                        "Do you want to save the progress?",
                        "Exit",
                        JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.YES_OPTION) {
                    fileSystem.saveState(jFrame);
                }
            }
        }

        @Override
        public void windowClosed(WindowEvent e) {

        }

        @Override
        public void windowIconified(WindowEvent e) {

        }

        @Override
        public void windowDeiconified(WindowEvent e) {

        }

        @Override
        public void windowActivated(WindowEvent e) {

        }

        @Override
        public void windowDeactivated(WindowEvent e) {

        }
    }
}

