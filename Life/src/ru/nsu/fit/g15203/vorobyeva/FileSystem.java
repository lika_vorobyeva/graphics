package ru.nsu.fit.g15203.vorobyeva;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class FileSystem {
    private static final String DEFAULT_FILENAME = "new.txt";
    private static final String DIRECTORY_SAVE = "FIT_15203_Vorobyeva_Lika_Life_Data/";
    public boolean saved = false;
    public String currentFilename = null;

    public int lineThickness;
    public int radius;
    public int numberOfColumns;
    public int numberOfLines;
    public Life life;
    private JFileChooser fileChooser = new JFileChooser();
    public boolean reload = false;

    public FileSystem(int numberOfColumns, int numberOfLines, int radius, int lineThickness, Life life) {
        this.numberOfColumns = numberOfColumns;
        this.numberOfLines = numberOfLines;
        this.lineThickness = lineThickness;
        this.radius = radius;
        this.life = life;

        fileChooser.setCurrentDirectory(new File(DIRECTORY_SAVE));
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
    }

    public boolean saveState(JFrame jFrame) {
        int living = 0;
        ArrayList<Point> array = new ArrayList<>(numberOfColumns * numberOfLines);

        int length = numberOfColumns - 1;
        for (int i = 0; i < numberOfLines; i++) {
            if (i % 2 == 0) {
                length++;
            } else {
                length--;
            }
            for (int j = 0; j < length; j++) {
                if (life.isLiving[i][j]) {
                    living++;
                    array.add(new Point(i, j));
                }
            }
        }

        File file = new File(DEFAULT_FILENAME);
        String fileName;

        fileChooser.setSelectedFile(file);
        if (!saved) {
            int ret = fileChooser.showSaveDialog(jFrame);

            if (ret == JFileChooser.APPROVE_OPTION) {
                file = fileChooser.getSelectedFile();
                fileName = file.getAbsolutePath();
                Path path = Paths.get(fileName);
                if (!path.toString().endsWith(".txt")) {
                    file = new File(fileName + ".txt");
                }
                currentFilename = file.getAbsolutePath();
            }
            else {
                return false;
            }
        } else {
            file = new File(currentFilename);
        }

        if (file.exists()) {
            file.delete();
        }

        try (Writer writer = new FileWriter(file)) {
            file.createNewFile();
            writer.write(numberOfColumns + " " + numberOfLines + "\n");
            writer.write(lineThickness + "\n");
            writer.write(radius + "\n");
            writer.write(living + "\n");

            for (int i = 0; i < array.size(); i++) {
                Point point = array.get(i);
                if (array.get(i) == null) {
                    break;
                } else {
                    writer.write(point.y + " " + point.x + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    public void loadFromFile(JFrame jFrame) {
        reload = false;
        File file = new File(DEFAULT_FILENAME);

        fileChooser.setSelectedFile(file);

        int ret = fileChooser.showOpenDialog(jFrame);

        if (ret == JFileChooser.APPROVE_OPTION) {
            file = fileChooser.getSelectedFile();
            currentFilename = file.getAbsolutePath();
            life.initLife();
        } else {
            return;
        }
        saved = true;

        try {
            int i = 0;
            Integer inputs[] = new Integer[5];
            BufferedReader scanner = new BufferedReader(new FileReader(file));
            Character character;
            int temp;
            while (i < 5) {
                 character = (char)scanner.read();
                 if(!character.equals('/')) {
                     if (Character.isDigit(character)) {
                         temp = Integer.parseInt(character.toString());
                         character = (char)scanner.read();
                             while (Character.isDigit(character)) {
                                 temp = 10 * temp + Integer.parseInt(character.toString());
                                 character = (char)scanner.read();
                             }

                         inputs[i] = temp;
                         i++;
                     }
                 }
                 else
                     scanner.readLine();
             }
            if (inputs[1] < 41) {
                if (inputs[1] > 0)
                    numberOfLines = inputs[1];
                else
                    return;
            }
            else
                numberOfLines = 40;
            if (inputs[0] < 41) {
                if (inputs[0] > 0)
                    numberOfColumns = inputs[0];
                else
                    return;
            }
            else
                numberOfColumns = 40;

            if (inputs[3] < 41) {
                if (inputs[3] > 7)
                    radius = inputs[3];
                else
                    radius = 8;
            }
            else
                radius = 40;
            if (inputs[2] < 11) {
                if (inputs[2] > 0)
                    lineThickness = inputs[2];
                else
                    return;
            }
            else
                lineThickness = 10;
            i = 0;

            life.isLiving = new boolean[numberOfLines][numberOfColumns];

            Point point = new Point(-1,-1);
            while (i < inputs[4]) {
                character = (char)scanner.read();
                if (!character.equals('/')) {
                    if (Character.isDigit(character)) {
                        temp = Integer.parseInt(character.toString());
                        character = (char)scanner.read();
                        while (Character.isDigit(character)) {
                            temp = 10 * temp + Integer.parseInt(character.toString());
                            character = (char)scanner.read();
                        }

                        point.x = temp;
                        while(!Character.isDigit(character))
                            character =  (char)scanner.read();
                        temp = Integer.parseInt(character.toString());
                        character =  (char)scanner.read();

                        while (Character.isDigit(character)) {
                            temp = 10 * temp + Integer.parseInt(character.toString());
                            character = (char)scanner.read();
                        }
                        point.y =  temp;
                        life.isLiving[point.y][point.x] = true;
                        i++;
                    }
                }
                else
                    scanner.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    catch (NoSuchElementException n) {
        n.printStackTrace();
    }
        reload = true;
    }
}
