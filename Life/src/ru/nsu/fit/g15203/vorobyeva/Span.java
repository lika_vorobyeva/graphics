package ru.nsu.fit.g15203.vorobyeva;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Stack;

public class Span {
    private Point first;
    private Point second;

    public Span(Point start, Point end) {
        this.first = start;
        this.second = end;
    }

    private Span getSpanLine(Point start, BufferedImage bufferedImage) {
        int leftX = start.x;
        int rightX = start.x;
        int y = start.y;

        while (bufferedImage.getRGB(leftX, y) != Color.BLACK.getRGB()) {
            leftX--;
        }
        leftX++;

        while (bufferedImage.getRGB(rightX, y) != Color.BLACK.getRGB()) {
            rightX++;
        }
        rightX--;

        Point left = new Point(leftX, y);
        Point right = new Point(rightX, y);

        return new Span(left, right);
    }


    public void fill(Point start, Color c, BufferedImage bufferedImage) {

        Stack<Span> stack = new Stack<>();
        Span span = getSpanLine(start, bufferedImage);
        stack.push(span);

        boolean coloredTop;
        boolean coloredDown;
        int y;

        Point first;
        Point second;
        Point temp;

        while (!stack.empty()) {
            span = stack.pop();
            first = span.first;
            second = span.second;
            coloredTop = false;
            coloredDown = false;

            for (int i = first.x; i <= second.x; i++) {
                bufferedImage.setRGB(i, first.y, c.getRGB());

                y = first.y - 1;
                if (bufferedImage.getRGB(i, y) != Color.BLACK.getRGB() && bufferedImage.getRGB(i, y) != c.getRGB()) {
                    if(!coloredTop) {
                        coloredTop = true;
                        temp = new Point(i, y);
                        stack.push(getSpanLine(temp, bufferedImage));
                    }
                }

                y = first.y + 1;
                if (bufferedImage.getRGB(i, y) != Color.BLACK.getRGB() && bufferedImage.getRGB(i, y) != c.getRGB()) {
                    if(!coloredDown) {
                        coloredDown = true;
                        temp = new Point(i, y);
                        stack.push(getSpanLine(temp, bufferedImage));
                    }
                }
            }
        }
    }
}